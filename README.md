# gastos de comida #

Se trata de una aplicación de Java para un mayor control sobre los gastos realizados cada día en la comida. Con esta aplicación se puede ver si mensualmente o semanalmente has ahorrado ó no, y/ó calcular los gastos realizados en cierta fecha.

### Estructura de la app ###

| Nombre                             | Descripción                                                 |
| ---------------------------------- |:-----------------------------------------------------------:|
| **archivos**/                      | Guarda los logs en los archivos externos.                   |
| **controlador**/                   | Intermediario entre el modelo y la vista.                   |
| **gestor**/                        | Son clases que contienen ArrayList o conexión a la BD.      |
| **modelo**/                        | Es la parte del servidor para obtener y manejar datos.      |
| **vista**/                         | El cliente visualiza la app.                                |
| main.java                          | Ejecuta la aplicación.                                      |




### Funcionamiento de la app: ###

    ClearDB → Base de datos en la nube MySQL.
    Controlador→ Controlar los datos de la base de datos y los que introduce el usuario.
    Vista → Visualizar datos para el usuario.

### Tecnologías a usar: ###

    - Java POO con el IDE Eclipse
    - MySQL → Base de datos relacional.

### Sistema operativo: ###

    - Windows 8.1 Pro