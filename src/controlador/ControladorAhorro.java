package controlador;

import vista.Ahorro;

public class ControladorAhorro {
	
	//Atributos de la clase
	private Ahorro ventanaAhorro;
	
	/**
     * Constructor sin parametros.
     */
	public ControladorAhorro(){
		// Se instancia la clase 
		ventanaAhorro=new Ahorro();
		// Coloca la ventana en el centro de la pantalla
		ventanaAhorro.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaAhorro.setVisible(true);
	}// Cierre constructor

} // Cierre de la clase