package controlador;

import vista.AhorroMensual;

public class ControladorAhorroMensual {
	
	//Atributos de la clase
	private AhorroMensual ventanaAhorroMensual;
	
	/**
     * Constructor sin parametros.
     */
	public ControladorAhorroMensual(){
		// Se instancia la clase 
		ventanaAhorroMensual=new AhorroMensual();
		// Coloca la ventana en el centro de la pantalla
		ventanaAhorroMensual.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaAhorroMensual.setVisible(true);
	}// Cierre constructor

} // Cierre de la clase