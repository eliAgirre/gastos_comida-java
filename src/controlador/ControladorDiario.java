package controlador;

import vista.Diario;

/**
 * Controla y muestra la ventana para ver los gastos diarios.
 * 
 */
public class ControladorDiario {
	
	//Atributos de la clase
	private Diario ventanaDiario;
	
	/**
     * Constructor sin parametros.
     */
	public ControladorDiario(){
		// Se instancia la clase 
		ventanaDiario=new Diario();
		// Coloca la ventana en el centro de la pantalla
		ventanaDiario.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaDiario.setVisible(true);
	}// Cierre constructor
	
} // Cierre de la clase