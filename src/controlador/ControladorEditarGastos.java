package controlador;

import vista.EditarGastos;

/**
 * Controla y muestra la ventana para editar los gastos de tienda.
 * 
 */
public class ControladorEditarGastos {
	
	//Atributos de la clase
	private EditarGastos ventanaEditarGastos;
	
	/**
     * Constructor con 1 parametro de tipo String.
     */
	public ControladorEditarGastos(String id, String fecha, String tienda, String coste, String formaPago, String tarjeta){
		// Instancia de la clase
		ventanaEditarGastos=new EditarGastos(id,fecha,tienda,coste,formaPago,tarjeta);
		// Coloca la ventana en el centro de la pantalla
		ventanaEditarGastos.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaEditarGastos.setVisible(true);
		
	} // Cierre del constructor

} // Cierre de la clase