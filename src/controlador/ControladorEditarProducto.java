package controlador;

import vista.EditarProducto;

public class ControladorEditarProducto {
	
	//Atributos de la clase
	private EditarProducto ventanaEditarProducto;
	
	/**
     * Constructor con 1 parametro de tipo String.
     */
	public ControladorEditarProducto(String id, String fecha, String tienda,String producto, String coste){
		// Instancia de la clase
		ventanaEditarProducto=new EditarProducto(id,fecha,tienda,producto,coste);
		// Coloca la ventana en el centro de la pantalla
		ventanaEditarProducto.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaEditarProducto.setVisible(true);
		
	} // Cierre del constructor

} // Cierre de la clase