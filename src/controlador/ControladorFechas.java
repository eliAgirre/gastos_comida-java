package controlador;

import vista.Concretos;

/**
 * 
 * Controla la ventana de fechas pasando una fecha o dos fechas.
 * 
 */
public class ControladorFechas {
	
	//Atributos de la clase
	private Concretos ventanaConcretos;
	
	/**
	 * 
     * Constructor con un parametro de tipo String.
     * 
     * @param fecha1 Es un String obtenido desde JOptionPane.
     * 
     */
	public ControladorFechas(String fecha1){
		
		// Se instancia la ventana y se le pasa una variable
		ventanaConcretos=new Concretos(fecha1);
		// Coloca la ventana en el centro de la pantalla
		ventanaConcretos.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaConcretos.setVisible(true);
	}// Cierre constructor
	
	/**
	 * 
     * Constructor con 2 parametros de tipo String.
     * 
     * @param fecha1 Es un String obtenido desde JOptionPane.
     * @param fecha2 Es un String obtenido desde JOptionPane.
     * 
     */
	public ControladorFechas(String fecha1,String fecha2){
		
		// Se instancia la ventana y se le pasa una variable
		ventanaConcretos=new Concretos(fecha1,fecha2);
		// Coloca la ventana en el centro de la pantalla
		ventanaConcretos.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaConcretos.setVisible(true);
	}// Cierre constructor

} // Cierre de la clase