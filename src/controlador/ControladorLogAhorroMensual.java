package controlador;

import vista.LogAhorroMensual;

public class ControladorLogAhorroMensual {
	
	//Atributos de la clase
	private LogAhorroMensual ventanaLogAhorroMensual;

	/**
     * Constructor sin parametros.
     */
	public ControladorLogAhorroMensual(){
		// Se instancia la clase 
		ventanaLogAhorroMensual=new LogAhorroMensual();
		// Coloca la ventana en el centro de la pantalla
		ventanaLogAhorroMensual.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaLogAhorroMensual.setVisible(true);
	} // Cierre constructor

} // Cierre de la clase