package controlador;

import vista.LogGastosDiarios;

public class ControladorLogGastosDiarios {
	
	//Atributos de la clase
	private LogGastosDiarios ventanaLogGastosDiarios;

	/**
     * Constructor sin parametros.
     */
	public ControladorLogGastosDiarios(){
		// Se instancia la clase 
		ventanaLogGastosDiarios=new LogGastosDiarios();
		// Coloca la ventana en el centro de la pantalla
		ventanaLogGastosDiarios.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaLogGastosDiarios.setVisible(true);
	} // Cierre constructor

} // Cierre de la clase