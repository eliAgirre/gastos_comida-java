package controlador;

import vista.LogGastosMensuales;

public class ControladorLogGastosMensuales {
	
	//Atributos de la clase
	private LogGastosMensuales ventanaLogGastosMensuales;

	/**
     * Constructor sin parametros.
     */
	public ControladorLogGastosMensuales(){
		// Se instancia la clase 
		ventanaLogGastosMensuales=new LogGastosMensuales();
		// Coloca la ventana en el centro de la pantalla
		ventanaLogGastosMensuales.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaLogGastosMensuales.setVisible(true);
	} // Cierre constructor

} // Cierre de la clase