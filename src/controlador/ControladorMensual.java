package controlador;

import vista.Mensual;

/**
 * Controla y muestra la ventana semanal.
 * 
 */
public class ControladorMensual {
	
	//Atributos de la clase
	private Mensual ventanaMensual;
	
	/**
     * Constructor sin parametros.
     */
	public ControladorMensual(){
		// Se instancia la clase 
		ventanaMensual=new Mensual();
		// Coloca la ventana en el centro de la pantalla
		ventanaMensual.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaMensual.setVisible(true);
	}// Cierre constructor

} // Cierre de la clase