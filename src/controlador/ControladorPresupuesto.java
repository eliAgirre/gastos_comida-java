package controlador;

import vista.Presupuesto;

/**
 * Controla y muestra la ventana para a�adir Presupuesto.
 * 
 */
public class ControladorPresupuesto {
	
	//Atributos de la clase
	private Presupuesto ventanaPresupuesto;
	
	/**
     * Constructor sin parametros.
     */
	public ControladorPresupuesto(){
		// Se instancia la clase 
		ventanaPresupuesto=new Presupuesto();
		// Coloca la ventana en el centro de la pantalla
		ventanaPresupuesto.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaPresupuesto.setVisible(true);
	}// Cierre constructor

} // Cierre de la clase