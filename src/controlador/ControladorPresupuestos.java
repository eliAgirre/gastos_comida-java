package controlador;

import vista.Presupuestos;

public class ControladorPresupuestos {
	
	//Atributos de la clase
	private Presupuestos ventanaPresupuestos;
	
	/**
     * Constructor sin parametros.
     */
	public ControladorPresupuestos(){
		// Se instancia la clase 
		ventanaPresupuestos=new Presupuestos();
		// Coloca la ventana en el centro de la pantalla
		ventanaPresupuestos.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaPresupuestos.setVisible(true);
	}// Cierre constructor

} // Cierre de la clase