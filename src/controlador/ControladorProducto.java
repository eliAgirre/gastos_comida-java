package controlador;

import vista.Producto;

/**
 * Controla y muestra la ventana para a�adir los gastos de producto.
 * 
 */
public class ControladorProducto {
	
	//Atributos de la clase
	private Producto ventanaProducto;
	
	/**
     * Constructor sin parametros.
     */
	public ControladorProducto() {
		// Se instancia la clase 
		ventanaProducto=new Producto();
		// Coloca la ventana en el centro de la pantalla
		ventanaProducto.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaProducto.setVisible(true);
		
	}// Cierre constructor

} // Cierre de la clase