package controlador;

import vista.Semanal;

/**
 * Controla y muestra la ventana semanal.
 * 
 */
public class ControladorSemanal {
	
	//Atributos de la clase
	private Semanal ventanaSemanal;
	
	/**
     * Constructor sin parametros.
     */
	public ControladorSemanal(){
		// Se instancia la clase 
		ventanaSemanal=new Semanal();
		// Coloca la ventana en el centro de la pantalla
		ventanaSemanal.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaSemanal.setVisible(true);
	}// Cierre constructor

} // Cierre de la clase