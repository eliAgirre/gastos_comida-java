package controlador;

import vista.TablaGastos;

/**
 * Controla y muestra la ventana tabla de gastos tienda a editar.
 * 
 */
public class ControladorTablaGastos {
	
	//Atributos de la clase
	private TablaGastos ventanaTablaGastos;
	
	/**
     * Constructor sin parametros.
     */
	public ControladorTablaGastos(){
		// Instancia de la clase
		ventanaTablaGastos=new TablaGastos();
		// Coloca la ventana en el centro de la pantalla
		ventanaTablaGastos.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaTablaGastos.setVisible(true);
		
	} // Cierre del constructor

} // Cierre de la clase