package controlador;

import vista.TablaProductos;

/**
 * Controla y muestra la ventana tabla de gastos de productos a editar.
 * 
 */
public class ControladorTablaProductos {
	
	//Atributos de la clase
	private TablaProductos ventanaTablaProductos;
	
	/**
     * Constructor sin parametros.
     */
	public ControladorTablaProductos(){
		// Instancia de la clase
		ventanaTablaProductos=new TablaProductos();
		// Coloca la ventana en el centro de la pantalla
		ventanaTablaProductos.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaTablaProductos.setVisible(true);
		
	} // Cierre del constructor

} // Cierre de la clase