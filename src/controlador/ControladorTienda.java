package controlador;

import vista.Tienda;

/**
 * Controla y muestra la ventana para a�adir los gastos de la tienda.
 * 
 */
public class ControladorTienda {
	
	//Atributos de la clase
	private Tienda ventanaTienda;

	/**
     * Constructor sin parametros.
     */
	public ControladorTienda() {
		
		ventanaTienda=new Tienda();
		// Coloca la ventana en el centro de la pantalla
		ventanaTienda.setLocationRelativeTo(null);
		// Hace visible la ventana
		ventanaTienda.setVisible(true);
		
	}// Cierre constructor	

} // Cierre de la clase