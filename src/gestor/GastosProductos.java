package gestor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import modelo.Gastos_P;

public class GastosProductos {
	
	// Atributos de la clase
	private ArrayList<Gastos_P> gastosProductos;
	private int id;
	private String fecha;
	private String tienda;
	private String producto;
	private double coste;
	
		// Atributos relacionados con la BD
		private static Statement stmt;
		private static ResultSet rs;
		private String sql;
	
	/**
	 * 
     * Constructor sin parametros, pero instancia un ArrayList.
     * 
     */
	public GastosProductos(){
		
		gastosProductos=new ArrayList<Gastos_P>();
	} // Cierre constructor
	
	/**
	 * 
     * A�ade cada gasto de producto al arraylist.
     * 
     */
	public void anadir(Gastos_P gastos){
		
		gastosProductos.add(gastos);
	}
	
	/**
	 * 
     * Obtiene el tama�o del arraylist.
     * 
     */
	public int size(){
		
		return gastosProductos.size();
	} // Cierre del metodo size
	
	/**
	 * 
     * Devuelve el ultimo id de la base de datos.
	 * @throws SQLException 
     * 
     */
	public int ultimoID() throws SQLException{
		
		// Sentencia SQL SELECT
		sql = "SELECT MAX(idGastosP) FROM gastos_producto;";
		rs = BD.consulta(stmt, sql);
		// Si hay datos
		if (rs.next()){
			id=rs.getInt(1);
        }
		// Devuelve el ultimo ID
		return id;		
	} // Cierre del metodo ultimoID
	
	/**
	 * 
     * Muestra los datos del arraylist.
     * 
     */
	public String listar(){     
         
        String datos="";
        String nl = System.getProperty("line.separator"); // Salto de linea
         
        // Se recorre el arraylist foreach
        for(Gastos_P gastos: gastosProductos){
             
            datos+=" Id: "+gastos.getIdGastosP()+nl;
            datos+=" Fecha: "+gastos.getFecha_compra()+nl;
            datos+=" Tienda: "+gastos.getTienda()+nl;
            datos+=" Tienda: "+gastos.getProducto()+nl;
            datos+=" Coste: "+gastos.getCoste()+" �"+nl;             
        }
        // Devuelve los datos del arraylist
        return datos;
        
    } // Cierre del metodo listar
	
	/**
	 * 
     * Inserta los datos del arraylist en la BD.
     * 
     */
	public void insertarBD(){
		// Se recorre el arraylist
		for(int i=0;i<gastosProductos.size();i++){
			
			id=gastosProductos.get(i).getIdGastosP();
			fecha=gastosProductos.get(i).getFecha_compra();
			tienda=gastosProductos.get(i).getTienda();
			producto=gastosProductos.get(i).getProducto();
			coste=gastosProductos.get(i).getCoste();
			// Sentencia INSERT
			sql="INSERT INTO gastos_producto VALUES ("+id+",'"+fecha+"','"+tienda+"','"+producto+"',"+coste+");";
			BD.actualizar(sql); //Ejecuta la actualizacion
			
		}		
	} // Cierre del metodo insertarBD

} // Cierre de la clase