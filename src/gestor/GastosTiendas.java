package gestor;

import java.sql.*;
import java.util.ArrayList;

import modelo.Gastos_T;

public class GastosTiendas {
	
	// Atributos de la clase
	private ArrayList<Gastos_T> gastosTiendas;
	private int id;
	private String fecha;
	private String tienda;
	private double coste;
	private String formaPago;
	private String nombreTarjeta;
	
		// Atributos relacionados con la BD
		private static Statement stmt;
		private static ResultSet rs;
		private String sql;
	
	/**
	 * 
     * Constructor sin parametros, pero instancia un ArrayList.
     * 
     */
	public GastosTiendas(){
		
		gastosTiendas=new ArrayList<Gastos_T>();		
	}// Cierre constructor
	
	/**
	 * 
     * A�ade cada gasto de tienda al arraylist.
     * 
     */
	 public void anadir(Gastos_T gastos){
	     
		 gastosTiendas.add(gastos);	         
	 }
	
	/**
	 * 
     * Obtiene el tama�o del arraylist.
     * 
     */
	public int size(){
		
		return gastosTiendas.size();
	} // Cierre del metodo size
	
	/**
	 * 
     * Devuelve el ultimo id de la base de datos.
	 * @throws SQLException 
     * 
     */
	public int ultimoID() throws SQLException{
		
		// Sentencia SQL SELECT
		sql = "SELECT MAX(idGastosT) FROM gastos_tienda;";
		rs = BD.consulta(stmt, sql);
		// Si hay datos
		if (rs.next()){
			id=rs.getInt(1);
        }
		// Devuelve el ultimo ID
		return id;		
	} // Cierre del metodo ultimoID
	
	/**
	 * 
     * Muestra los datos del arraylist.
     * 
     */
	public String listar(){     
         
        String datos="";
        String nl = System.getProperty("line.separator"); // Salto de linea
         
        // Se recorre el arraylist foreach
        for(Gastos_T gastos: gastosTiendas){
             
            datos+=" Id: "+gastos.getIdGastos()+nl;
            datos+=" Fecha: "+gastos.getFecha()+nl;
            datos+=" Tienda: "+gastos.getNombreTienda()+nl;
            datos+=" Coste: "+gastos.getCosteCompra()+" �"+nl;         
            datos+=" Forma de pago: "+gastos.getFormaPago()+nl;
            datos+=" Tarjeta de : "+gastos.getNombreTarjeta()+nl;
        }
        // Devuelve los datos del arraylist
        return datos;
        
    } // Cierre del metodo listar
	
	/**
	 * 
     * Inserta los datos del arraylist en la BD.
     * 
     */
	public void insertarBD(){
		// Se recorre el arraylist
		for(int i=0;i<gastosTiendas.size();i++){
			
			// Se obtienen los datos de la clase Gastos_T
			id=gastosTiendas.get(i).getIdGastos();
			fecha=gastosTiendas.get(i).getFecha();
			tienda=gastosTiendas.get(i).getNombreTienda();
			coste=gastosTiendas.get(i).getCosteCompra();
			formaPago=gastosTiendas.get(i).getFormaPago();
			nombreTarjeta=gastosTiendas.get(i).getNombreTarjeta();
			
			// Sentencia INSERT
			sql="INSERT INTO gastos_tienda VALUES ("+id+",'"+fecha+"','"+tienda+"',"+coste+",'"+formaPago+"','"+nombreTarjeta+"');";
			BD.actualizar(sql); //Ejecuta la actualizacion
			
		}		
	} // Cierre del metodo insertarBD
	
} // Cierre de la clase