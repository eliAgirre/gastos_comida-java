package modelo;

public class Gastos_P {
	
	// Atributos de clase
	private int idGastosP;
	private String fecha_compra;
	private String tienda;
	private String producto;
	private double coste;
	
	public Gastos_P(int idGastosP, String fecha_compra, String tienda, String producto, double coste) {
		
		this.idGastosP = idGastosP;
		this.fecha_compra = fecha_compra;
		this.tienda = tienda;
		this.producto = producto;
		this.coste = coste;
	}
	// Getters
	public int getIdGastosP(){
		return idGastosP;
	}
	public String getFecha_compra() {
		return fecha_compra;
	}
	public String getTienda() {
		return tienda;
	}
	public String getProducto() {
		return producto;
	}
	public double getCoste() {
		return coste;
	}
	
	// Setters
	public void setIdGastosP(int idGastosP){
		this.idGastosP = idGastosP;
	}
	public void setFecha_compra(String fecha_compra) {
		this.fecha_compra = fecha_compra;
	}
	public void setTienda(String tienda) {
		this.tienda = tienda;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public void setCoste(double coste) {
		this.coste = coste;
	}
	
} // Cierre clase