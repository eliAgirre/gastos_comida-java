package modelo;

public class Gastos_T {
	
	//Atributos de la clase
	private int idGastos;
	private String fecha;
	private String nombreTienda;
	private double costeCompra;
	private String formaPago;
	private String nombreTarjeta;
	
	/**
	 * 
     * Constructor con todos los parametros.
     * 
     */
	public Gastos_T(int idGastos, String fecha, String nombreTienda, double costeCompra, String formaPago, String nombreTarjeta) {
		
		this.idGastos = idGastos;
		this.fecha = fecha;
		this.nombreTienda = nombreTienda;
		this.costeCompra = costeCompra;
		this.formaPago = formaPago;
		this.nombreTarjeta = nombreTarjeta;
		
	} // Cierre constructor

	// Getters
	public int getIdGastos() {
		return idGastos;
	}

	public String getFecha() {
		return fecha;
	}

	public String getNombreTienda() {
		return nombreTienda;
	}

	public double getCosteCompra() {
		return costeCompra;
	}
	
	public String getFormaPago() {
		return formaPago;
	}

	public String getNombreTarjeta() {
		return nombreTarjeta;
	}

	// Setters
	public void setIdGastos(int idGastos) {
		this.idGastos = idGastos;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public void setNombreTienda(String nombreTienda) {
		this.nombreTienda = nombreTienda;
	}

	public void setCosteCompra(double costeCompra) {
		this.costeCompra = costeCompra;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public void setNombreTarjeta(String nombreTarjeta) {
		this.nombreTarjeta = nombreTarjeta;
	}
	
} // Cierre de la clase