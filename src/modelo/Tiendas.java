package modelo;

public class Tiendas {

	// Atributos de la clase
	private int idTienda;
	private String nombre;
	
	/**
	 * Constructor con parametros todos los atributos.
	 * 
	 * @param idTienda Es un identificador de la tienda.
	 * @param nombre Nombre de la tienda.
	 * 
    */
	public Tiendas(int idTienda, String nombre) {

		this.idTienda = idTienda;
		this.nombre = nombre;
	}// Cierre constructor

	// Getters
	public int getIdTienda() {
		return idTienda;
	}

	public String getNombre() {
		return nombre;
	}
	
	/**
	 * El nombre de la tienda aparece en el JComboBox.
	 * 
	 * @return nombre Devuelve el nombre de la tienda.
	 * 
    */
	@Override
	public String toString() {
		return this.nombre;
	} // Cierre del metodo toString	
	
} // Cierre de la clase