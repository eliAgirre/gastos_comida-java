package vista;

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import controlador.ControladorPrincipal;
import gestor.BD;

@SuppressWarnings("serial")
public class Ahorro extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private JTextField txtFechaInicio;
	private JTextField txtFechaFin;
	private String fechaInicio;
	private String fechaFin;
	private Double presu;
	private Double gastos;
	private JButton btnVer;
	private JButton btnGuardar;
	private JButton btnMenu;
	private int id=0;
	
		// Atributos relacionados con la tabla
		private JScrollPane scrollPane;
		private DefaultTableModel modelo;
		private String [] vector={"fecha_inicio","fecha_fin","presu","gasto","ahorro"}; //cabecera
		private String [][] arrayTabla; //array bidimensional
		private JTable tabla;
		private static String[] datos=new String[5];
		
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
		private boolean tablaVacia;
	
	/**
	 * 
     * Constructor sin parametros y contiene el metodo componentes.
     * 
     */
	public Ahorro(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 581, 566); // tama�o
		setTitle("Ahorro semanal"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/coins.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
	
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();	
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
		
	} // Cierre del constructor
	
	/**
	 * 
     * Contiene todos los componentes de la ventana Diario.
     *  
     */
	public void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(10, 512, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Introduzca las fechas para saber el ahorro semanal.");
		lblAviso.setBounds(26, 23, 297, 20);
		contentPane.add(lblAviso);
		
		JLabel lblFecha = new JLabel("Fecha inicio:");
		lblFecha.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFecha.setForeground(new Color(0, 139, 139));
		lblFecha.setBounds(85, 76, 76, 14);
		contentPane.add(lblFecha);
		
		txtFechaInicio = new JTextField();
		txtFechaInicio.setBounds(176, 73, 111, 20);
		contentPane.add(txtFechaInicio);
		txtFechaInicio.setColumns(10);
		
		JLabel lblFecha2 = new JLabel("Fecha fin:");
		lblFecha2.setForeground(new Color(0, 139, 139));
		lblFecha2.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFecha2.setBounds(98, 107, 63, 14);
		contentPane.add(lblFecha2);
		
		txtFechaFin = new JTextField();
		txtFechaFin.setColumns(10);
		txtFechaFin.setBounds(176, 104, 111, 20);
		contentPane.add(txtFechaFin);
		
		btnVer = new JButton("Consultar");
		btnVer.addActionListener((ActionListener)this);
		btnVer.setBackground(new Color(184, 231, 255));
		btnVer.setBounds(362, 105, 128, 41);
		btnVer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/view.png")));
		contentPane.add(btnVer);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener((ActionListener)this);
		btnGuardar.setBackground(new Color(184, 231, 255));
		btnGuardar.setBounds(362, 53, 128, 41);
		btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/save.png")));
		contentPane.add(btnGuardar);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(249, 493, 111, 33);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);
		
		// Panel para visualizar y hacer scroll
		scrollPane = new JScrollPane();
		scrollPane.setEnabled(false);
		scrollPane.setBounds(67, 168, 462, 303);
		contentPane.add(scrollPane);
		
		// Modelo tabla, contiene cabecera y arrayBidimensional para los datos
		modelo = new DefaultTableModel(arrayTabla,vector);
		
		// Se le pasa a JTable el modelo de tabla
		tabla = new JTable(modelo);
		tabla.setEnabled(false);
		scrollPane.setViewportView(tabla);// Se a�ade la tabla al panel scroll
		
	} // Cierre del metodo componentes
	
	/**
	 * 
     * Limpia los datos de los JTextFields.
     *  
     */
	public void limpiar(){
		txtFechaInicio.setText("");
		txtFechaFin.setText("");
	}

	/**
     * Metodo para que los componentes de la ventana realicen una accion concreta.
     *  
     */
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {	
		// Si hace clic en "Guardar" se visualiza en la tabla y guarda los datos en la BD
		if(evento.getSource()==btnGuardar){
			try {
				// Se obtienen los datos de JTextFields
				fechaInicio=txtFechaInicio.getText().trim();
				fechaFin=txtFechaFin.getText().trim();
				// Sentencia SELECT presu
				sql = "SELECT cantidad FROM presupuestos WHERE fecha_presu='"+fechaInicio+"' and tipo='Semanal';";
				// Hace la consulta y devuelve el resultado			
				rs = BD.consulta(stmt,sql);
				// Mientras que haya datos
				while (rs.next()) {
					// Se guardan los datos en array
					datos[0]=fechaInicio;
					datos[1]=fechaFin;
					datos[2]=Double.toString(rs.getDouble(1));
					presu=rs.getDouble(1);
				} // Cierre de while				
				
				// Sentencia SELECT gasto
				sql = "SELECT SUM(costeCompraT) FROM gastos_tienda WHERE fechaGT>='"+fechaInicio+"' and fechaGT<='"+fechaFin+"'";
				// Hace la consulta y devuelve el resultado			
				rs = BD.consulta(stmt,sql);
				// Mientras que haya datos
				while (rs.next()) {
					datos[3]=Double.toString(rs.getDouble(1));
					gastos=rs.getDouble(1);
					datos[4]=Double.toString(presu-gastos);
					// Se a�ade cada fila al modelo de tabla
					modelo.addRow(datos);
				} // Cierre de while
				
				rs.close(); //Cierre de la consulta	
				// Limpia los TextFields
				limpiar();
				// Sentencia SQL SELECT
				sql = "SELECT * FROM ahorro_semanal;";
				// Comprueba si la tabla esta vacia
				tablaVacia=BD.tablaVacia(stmt,sql);
				// Si la tabla esta vacia
				if(tablaVacia==true){
					id++;					
					// Sentencia INSERT
					sql = "INSERT INTO ahorro_semanal VALUES("+id+",'"+datos[0]+"','"+datos[1]+"',"+datos[2]+","+datos[3]+","+datos[4]+");";
					// Se insertan los datos en la BD
					BD.actualizar(sql);
					// Muestra un aviso para el usuario
					JOptionPane.showMessageDialog(null, "Los datos han sido guardados correctamente.");
				}
				else{
					
					// Sentencia SQL
					sql = "SELECT MAX(idAhorro) FROM ahorro_semanal;";
					// Ejecuta la consulta
					rs = BD.consulta(stmt, sql);
					// Si hay datos
					if (rs.next()){
						// Se obtiene el ultimo ID desde la BD
						id=rs.getInt(1);
			        }					
					id++;
					// Sentencia INSERT
					sql = "INSERT INTO ahorro_semanal VALUES("+id+",'"+datos[0]+"','"+datos[1]+"',"+datos[2]+","+datos[3]+","+datos[4]+");";
					// Se insertan los datos en la BD
					BD.actualizar(sql);
					// Muestra un aviso para el usuario
					JOptionPane.showMessageDialog(null, "Los datos han sido guardados correctamente.");					
				} // Cierre del else
			}
			catch (SQLException e) {
				// Muestra error SQL
				JOptionPane.showMessageDialog(null, e.getMessage());
				
			} // Cierre excepcion
		}
		// Si hace clic en "Ver" visualizara los gastos de esos dias
		if(evento.getSource()==btnVer){ 
			try {
				// Se obtienen los datos de JTextFields
				fechaInicio=txtFechaInicio.getText().trim();
				fechaFin=txtFechaFin.getText().trim();
				// Sentencia SELECT presu
				sql = "SELECT cantidad FROM presupuestos WHERE fecha_presu='"+fechaInicio+"' and tipo='Semanal';";
				// Hace la consulta y devuelve el resultado			
				rs = BD.consulta(stmt,sql);
				// Mientras que haya datos
				while (rs.next()) {
					// Se guardan los datos en array
					datos[0]=fechaInicio;
					datos[1]=fechaFin;
					datos[2]=Double.toString(rs.getDouble(1));
					presu=rs.getDouble(1);
				} // Cierre de while
				
				// Sentencia SELECT gasto
				sql = "SELECT SUM(costeCompraT) FROM gastos_tienda WHERE fechaGT>='"+fechaInicio+"' and fechaGT<='"+fechaFin+"'";
				// Hace la consulta y devuelve el resultado			
				rs = BD.consulta(stmt,sql);
				// Mientras que haya datos
				while (rs.next()) {
					datos[3]=Double.toString(rs.getDouble(1));
					gastos=rs.getDouble(1);
					datos[4]=Double.toString(presu-gastos);
					// Se a�ade cada fila al modelo de tabla
					modelo.addRow(datos);
				} // Cierre de while
				
				rs.close(); //Cierre de la consulta	
				// Limpia los TextFields
				limpiar();
			}
			catch (SQLException e) {
				// Muestra error SQL
				JOptionPane.showMessageDialog(null, e.getMessage());
				
			} // Cierre excepcion
		}
		// Si hace clic en Menu vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}// Cierre evento btnMenu
		
	} // Cierre del metodo actionPerformed
} // Cierre de la clase