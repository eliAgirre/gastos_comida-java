package vista;

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import controlador.ControladorPrincipal;
import gestor.BD;

@SuppressWarnings("serial")
public class Diario extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private DefaultTableModel modelo;
	private String [] vector={"id", "fecha","tienda","coste","forma de pago", "tarjeta"}; //cabecera
	private String [][] arrayTabla; //array bidimensional
	private JTable tabla;
	private static String[] datosBD=new String[6];
	private JComboBox<Object> cbTiendas;
	private String[] listaTiendas = {"Eroski","Okela","Sustrai","OgiBerri","Vincen","Dia","Otro","Garin"}; //Lista de array para combobox
	private JButton btnVer;
	private JButton btnMenu;	
	
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
	
	/**
	 * 
     * Constructor sin parametros y contiene el metodo componentes.
     * 
     */
	public Diario(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 599, 486); // tama�o
		setTitle("Ver gastos diarios"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/view.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();	
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();			
			// Sentencia SQL SELECT
			sql = "SELECT * FROM gastos_tienda;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			// Mientras que haya datos
			while (rs.next()) { 
				
				// Se guardan los datos en array datosBD
				datosBD[0]=Integer.toString(rs.getInt(1));
				datosBD[1]=rs.getString(2);
				datosBD[2]=rs.getString(3);
				datosBD[3]=Double.toString(rs.getDouble(4))+" �";
				datosBD[4]=rs.getString(5);
				datosBD[5]=rs.getString(6);
				// Se a�ade cada fila al modelo de tabla
				modelo.addRow(datosBD);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
	} // Cierre constructor
	
	/**
	 * 
     * Contiene todos los componentes de la ventana Diario.
     *  
     */
	public void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Panel para visualizar y hacer scroll
		scrollPane = new JScrollPane();
		scrollPane.setEnabled(false);
		scrollPane.setBounds(10, 99, 573, 303);
		contentPane.add(scrollPane);
		
		// Modelo tabla, contiene cabecera y arrayBidimensional para los datos
		modelo = new DefaultTableModel(arrayTabla,vector);
		
		// Se le pasa a JTable el modelo de tabla
		tabla = new JTable(modelo);
		tabla.setEnabled(false);
		scrollPane.setViewportView(tabla);// Se a�ade la tabla al panel scroll
		
		// Label version
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 432, 95, 14);
		contentPane.add(lblVersion);
		
		// Boton menu
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(245, 413, 106, 33);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);
		
		JLabel lblPuedeSeleccionarLa = new JLabel("Puede seleccionar la lista de tiendas:");
		lblPuedeSeleccionarLa.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPuedeSeleccionarLa.setBounds(10, 46, 239, 14);
		contentPane.add(lblPuedeSeleccionarLa);
		
 		// JComboBox obtiene la lista de array de tiendas
		cbTiendas = new JComboBox<Object>(listaTiendas);
		cbTiendas.setBounds(245, 44, 147, 20);
		contentPane.add(cbTiendas);
		
		btnVer = new JButton("Consultar");
		btnVer.addActionListener((ActionListener)this);
		btnVer.setBounds(443, 34, 140, 40);
		btnVer.setBackground(new Color(184, 231, 255));
		btnVer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/view.png")));
		contentPane.add(btnVer);
		
	} // Cierre del metodo componentes
	
	/**
     * Metodo para limpiar los datos de la tabla.
     *  
     */
	public void limpiarTabla() {
		
		modelo.setRowCount(0);
	} // Cierre de limpiarTabla
	
	/**
     * Metodo para que los componentes de la ventana realicen una accion concreta.
     *  
     */
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {	
		// Si hace clic en "Consultar" visualizara los gastos de una tienda en concreto
		if(evento.getSource()==btnVer){
			// Limpia la tabla
			limpiarTabla();
			// Se hace la conexion con la BD
			try {		
				// Sentencia SQL SELECT
				sql = "SELECT * FROM gastos_tienda WHERE tienda='"+cbTiendas.getSelectedItem()+"';";
				// Hace la consulta y devuelve el resultado
				rs = BD.consulta(stmt,sql);
				// Mientras que haya datos
				while (rs.next()) { 
					
					// Se guardan los datos en array datosBD
					datosBD[0]=Integer.toString(rs.getInt(1));
					datosBD[1]=rs.getString(2);
					datosBD[2]=rs.getString(3);
					datosBD[3]=Double.toString(rs.getDouble(4))+" �";
					datosBD[4]=rs.getString(5);
					datosBD[5]=rs.getString(6);
					// Se a�ade cada fila al modelo de tabla
					modelo.addRow(datosBD);
					
				} // Cierre de while
				
				rs.close(); //Cierre de la consulta
			}
			catch (SQLException e) {
				// Muestra error SQL
				JOptionPane.showMessageDialog(null, e.getMessage());
				
			} // Cierre excepcion
		}
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}// Cierre evento btnMenu
		
	} // Cierre del metodo actionPerformed
} //Cierre de la clase