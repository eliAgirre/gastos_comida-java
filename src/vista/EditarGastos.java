package vista;

import java.awt.*;
import java.awt.event.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;

import javax.swing.*;
import javax.swing.border.*;

import controlador.ControladorPrincipal;
import controlador.ControladorTablaGastos;
import gestor.BD;

@SuppressWarnings("serial")
public class EditarGastos extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private JTextField txtFecha;
	private JLabel lblTienda;
	private String[] listaTiendas = {"Eroski","Okela","Sustrai","OgiBerri","Vincen","Dia","Otro","Garin"}; //Lista de array para combobox
	private JComboBox<Object> cbTienda;
	private JTextField txtCoste;
	private JButton btnVolver;
	private JButton btnMenu;
	private JTextField txtID;
	private int id;
	private String fecha;
	private String tienda;
	private double coste;
	private JButton btnBorrar;
	private String sql;
	private JLabel lblFormaPago;
	private JRadioButton rbEfectivo;
	private JRadioButton rbTarjeta;
	private final ButtonGroup pagoGroup = new ButtonGroup();
	private JLabel lblTarjeta;
	private JRadioButton rbAmaia;
	private JRadioButton rbPeio;
	private final ButtonGroup tarjetaGroup = new ButtonGroup();
	private String formaPago;
	private String nombreTarjeta;
	
	/**
	 * 
     * Constructor con 4 parametros para mostrar los datos en los componentes de la ventana.
     * 
     */
	public EditarGastos(String id, String fecha, String tienda, String coste, String formaPago, String tarjeta){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 410, 469); // tama�o
		setTitle("Editar gastos de tienda"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/edit.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Llama al metodo componentes
		componentes();
		
		// Establece los textos de JTextFields + JComboBox + Radio buttons
		txtID.setText(id);
		txtFecha.setText(fecha);
		cbTienda.setSelectedItem(tienda);
		txtCoste.setText(coste.substring(0,6));
		// Si la forma de pago es en Efectivo
		if(formaPago.equals("Efectivo")){
			// Muestra seleccionado el efectivo
			rbEfectivo.setSelected(true);
			rbTarjeta.setSelected(false);
			rbAmaia.setEnabled(false); // Inhabilita los radio buttons
			rbPeio.setEnabled(false);
		}
		else{
			// Muestra seleccionado la tarjeta
			rbEfectivo.setSelected(false);
			rbTarjeta.setSelected(true);
			rbAmaia.setEnabled(true); // Habilita los radio buttons
			rbPeio.setEnabled(true);
			// Si la tarjeta es de Amaia
			if(formaPago.equals("Amaia")){
				// Muestra seleccionado "Amaia"
				rbAmaia.setSelected(true);
			}
			else{
				// Muestra seleccionado "Peio"
				rbPeio.setSelected(true);
			}
		} // Cierre del else
		
	} // Cierre del constructor
	
	/**
	 * 
     * Contiene todos los componentes de la ventana Tienda.
     *  
     */
	public void componentes (){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(10, 415, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblID = new JLabel("ID:");
		lblID.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblID.setForeground(new Color(0, 139, 139));
		lblID.setBounds(85, 34, 48, 14);
		contentPane.add(lblID);
		
		txtID = new JTextField();
		txtID.setEnabled(false);
		txtID.setColumns(10);
		txtID.setBounds(141, 31, 126, 20);
		contentPane.add(txtID);
		
		JLabel lblFecha = new JLabel("Fecha:");
		lblFecha.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFecha.setForeground(new Color(0, 139, 139));
		lblFecha.setBounds(85, 81, 46, 14);
		contentPane.add(lblFecha);
		
		txtFecha = new JTextField();
		txtFecha.setBounds(141, 78, 126, 20);
		contentPane.add(txtFecha);
		txtFecha.setColumns(10);
		
		lblTienda = new JLabel("Tienda:");
		lblTienda.setForeground(new Color(0, 139, 139));
		lblTienda.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTienda.setBounds(85, 125, 46, 14);
		contentPane.add(lblTienda);
		
		// JComboBox obtiene la lista de array de tiendas
		cbTienda = new JComboBox<Object>(listaTiendas); 
		cbTienda.setBounds(141, 122, 126, 20);
		contentPane.add(cbTienda);
		
		JLabel lblCoste = new JLabel("Coste:");
		lblCoste.setForeground(new Color(0, 139, 139));
		lblCoste.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCoste.setBounds(85, 175, 46, 14);
		contentPane.add(lblCoste);
		
		txtCoste = new JTextField();
		txtCoste.setColumns(10);
		txtCoste.setBounds(141, 172, 126, 20);
		contentPane.add(txtCoste);
		
		lblFormaPago = new JLabel("Forma de pago:");
		lblFormaPago.setForeground(new Color(0, 139, 139));
		lblFormaPago.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFormaPago.setBounds(85, 225, 104, 14);
		contentPane.add(lblFormaPago);
		
		rbEfectivo = new JRadioButton("Efectivo");
		rbEfectivo.addActionListener((ActionListener)this);
		rbEfectivo.setBounds(199, 211, 109, 23);
		pagoGroup.add(rbEfectivo);
		contentPane.add(rbEfectivo);
		
		rbTarjeta = new JRadioButton("Tarjeta");	
		rbTarjeta.addActionListener((ActionListener)this);
		rbTarjeta.setBounds(199, 238, 109, 23);
		pagoGroup.add(rbTarjeta);
		contentPane.add(rbTarjeta);
		
		lblTarjeta = new JLabel("Tarjeta:");
		lblTarjeta.setForeground(new Color(0, 139, 139));
		lblTarjeta.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTarjeta.setBounds(85, 293, 104, 14);
		contentPane.add(lblTarjeta);
		
		rbAmaia = new JRadioButton("Amaia");
		rbAmaia.addActionListener((ActionListener)this);
		rbAmaia.setBounds(199, 278, 109, 23);
		tarjetaGroup.add(rbAmaia);
		contentPane.add(rbAmaia);
		
		rbPeio = new JRadioButton("Peio");
		rbPeio.addActionListener((ActionListener)this);
		rbPeio.setBounds(199, 304, 109, 23);
		tarjetaGroup.add(rbPeio);
		contentPane.add(rbPeio);
		
		btnVolver = new JButton("Volver");
		btnVolver.addActionListener((ActionListener)this);
		btnVolver.setBounds(25, 363, 108, 41);
		btnVolver.setBackground(new Color(184, 231, 255));
		btnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/back.png")));
		contentPane.add(btnVolver);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(143, 363, 114, 41);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);		
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener((ActionListener)this);
		btnBorrar.setBackground(new Color(184, 231, 255));
		btnBorrar.setBounds(267, 363, 111, 41);
		btnBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/borrar.png")));
		contentPane.add(btnBorrar);
		
	} // Cierre del metodo componentes
	
	/**
     * Obtiene la fecha de sistema.
     * 
     * @return ts Devuelve un Timestamp.
     * 
     */
	private static Timestamp obtenerFechaSistema(){
		
		//creamos el objeto calendar y obtenemos la hora del sistema
		Calendar cal = Calendar.getInstance();
		
		//Se convierte el objeto calendar a timestamp
		Timestamp ts = new Timestamp(cal.getTimeInMillis()); 
		
		return ts;
		
	} //Cierre del m�todo obtenerHoraSistema
	
	/**
	 * 
     * Guarda los datos en un fichero log.
     *  
     */
	public static void gastosLog(String evento, int idGastos, String fecha_compra, String tienda, String gasto){
		
		try {
			
			//FileWriter ficheroW=new FileWriter("src/archivos/gastos.log",true);
			FileWriter ficheroW=new FileWriter("archivos/gastos.log",true);
			BufferedWriter bW=new BufferedWriter(ficheroW);
			
			Timestamp hora=obtenerFechaSistema();
			
			String datos=evento+"   "+hora +"   "+Integer.toString(idGastos)+"   "+fecha_compra+ "    "+tienda+ "      "+gasto;

			bW.write(datos);
			bW.newLine();
			bW.newLine();
			bW.close();
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	} //Cierre del metodo gastosLog
		
	/**
     * Metodo para que los componentes de la ventana realicen una accion concreta.
     *  
     */
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {	
		// Si ha seleccionado radio "Efectivo"
		if(evento.getSource()==rbEfectivo){
			// Se establece "Efectivo" el valor de la variable 
			formaPago=rbEfectivo.getText();
			rbAmaia.setEnabled(false);
			rbPeio.setEnabled(false);
			nombreTarjeta="";
			
		}
		// Si ha seleccionado radio "Tarjeta"
		if(evento.getSource()==rbTarjeta){
			// Se establece "Tarjeta" el valor de la variable 
			formaPago=rbTarjeta.getText();
			rbAmaia.setEnabled(true);
			rbPeio.setEnabled(true);
			
		}
		// Si ha seleccionado radio "Amaia"
		if(evento.getSource()==rbAmaia){
			// Se establece "Amaia" el valor de la variable 
			nombreTarjeta=rbAmaia.getText();
			
		}
		// Si ha seleccionado radio "Peio"
		if(evento.getSource()==rbPeio){
			// Se establece "Peio" el valor de la variable 
			nombreTarjeta=rbPeio.getText();
			
		}
		// Si hace clic en Volver y vuelve a la ventana TablaGastos
		if(evento.getSource()==btnVolver){
			// Se instancia el controlador TablaGastos
			ControladorTablaGastos controladorEditarGastos=new ControladorTablaGastos();
	  		// Desparece la ventana editar Gastos de Tienda
	  		setVisible(false);
		}
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			// Obtener los datos de cada componente de la ventana
			id=Integer.parseInt(txtID.getText());
			fecha=txtFecha.getText().trim();
			tienda=cbTienda.getSelectedItem().toString().trim();
			coste=Double.parseDouble(txtCoste.getText());
			//Sentencia UPDATE
			sql = "UPDATE gastos_tienda SET idGastosT="+id+",fechaGT = '"+fecha+"',tienda = '"+tienda+"',costeCompraT = '"+coste+"',forma_pago = '"+formaPago+"',tarjeta = '"+nombreTarjeta+"' WHERE idGastosT='"+id+"'";
			// Ejecuta la actualizacion
			BD.actualizar(sql);
			// Guarda el evento realizado en fichero log
			gastosLog("Editar",id,fecha,tienda,Double.toString(coste));
			// Muestra un aviso al usuario
			JOptionPane.showMessageDialog(null, "Los datos se ha editado correctamente.");
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece la ventana editar Gastos de Tienda
	  		setVisible(false);
		}
		if(evento.getSource()==btnBorrar){ 
			
			// Obtener el id de la ventana
			id=Integer.parseInt(txtID.getText());
			// Sentencia DELETE
			sql="DELETE FROM gastos_tienda WHERE idGastosT=?;";
			BD.borrarRegistro(sql,id); // Elimina el registro
			// Guarda el evento realizado en fichero log
			gastosLog("Borrar",id," "," "," ");
			// Muestra aviso al usuario
			JOptionPane.showMessageDialog(null, "El gasto ha sido borrado.");
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece la ventana editar Gastos de Tienda
	  		setVisible(false);
			
		} // Cierre del btnBorrar
		
	} // Cierre del metodo actionPerformed
} // Cierre de la clase