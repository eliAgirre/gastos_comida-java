package vista;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import controlador.ControladorPrincipal;
import controlador.ControladorTablaProductos;
import gestor.BD;

@SuppressWarnings("serial")
public class EditarProducto extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private JTextField txtFecha;
	private JLabel lblTienda;
	private String[] listaTiendas = {"Eroski","Okela","Sustrai","Panaderia","Vincen","Dia"}; //Lista de array para combobox
	private JComboBox<Object> cbTienda;
	private JTextField txtCoste;
	private JButton btnVolver;
	private JButton btnMenu;
	private JTextField txtID;
	private int id;
	private String fecha;
	private String tienda;
	private String producto;
	private double coste;
	private JButton btnBorrar;
	private String sql;
	private JTextField txtProducto;
	
	/**
	 * 
     * Constructor con 4 parametros para mostrar los datos en los componentes de la ventana.
     * 
     */
	public EditarProducto(String id, String fecha, String tienda, String producto, String coste){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 382, 366); // tama�o
		setTitle("Editar gastos de producto"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/edit.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Llama al metodo componentes
		componentes();
		
		// Establece los textos de JTextFields + JComboBox
		txtID.setText(id);
		txtFecha.setText(fecha);
		cbTienda.setSelectedItem(tienda);
		txtProducto.setText(producto);
		txtCoste.setText(coste.substring(0,5));
		
	} // Cierre del constructor
	
	/**
	 * 
     * Contiene todos los componentes de la ventana Tienda.
     *  
     */
	public void componentes (){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 313, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblID = new JLabel("ID:");
		lblID.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblID.setForeground(new Color(0, 139, 139));
		lblID.setBounds(43, 34, 48, 14);
		contentPane.add(lblID);
		
		txtID = new JTextField();
		txtID.setEnabled(false);
		txtID.setColumns(10);
		txtID.setBounds(129, 34, 126, 20);
		contentPane.add(txtID);
		
		JLabel lblFecha = new JLabel("Fecha:");
		lblFecha.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFecha.setForeground(new Color(0, 139, 139));
		lblFecha.setBounds(43, 81, 46, 14);
		contentPane.add(lblFecha);
		
		txtFecha = new JTextField();
		txtFecha.setBounds(129, 81, 126, 20);
		contentPane.add(txtFecha);
		txtFecha.setColumns(10);
		
		lblTienda = new JLabel("Tienda:");
		lblTienda.setForeground(new Color(0, 139, 139));
		lblTienda.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTienda.setBounds(43, 125, 46, 14);
		contentPane.add(lblTienda);
		
		// JComboBox obtiene la lista de array de tiendas
		cbTienda = new JComboBox<Object>(listaTiendas); 
		cbTienda.setBounds(129, 125, 126, 20);
		contentPane.add(cbTienda);
		
		JLabel lblProducto = new JLabel("Producto:");
		lblProducto.setForeground(new Color(0, 139, 139));
		lblProducto.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblProducto.setBounds(43, 168, 76, 14);
		contentPane.add(lblProducto);
		
		txtProducto = new JTextField();
		txtProducto.setColumns(10);
		txtProducto.setBounds(129, 168, 126, 20);
		contentPane.add(txtProducto);
		
		JLabel lblCoste = new JLabel("Coste:");
		lblCoste.setForeground(new Color(0, 139, 139));
		lblCoste.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCoste.setBounds(43, 209, 46, 14);
		contentPane.add(lblCoste);
		
		txtCoste = new JTextField();
		txtCoste.setColumns(10);
		txtCoste.setBounds(129, 209, 126, 20);
		contentPane.add(txtCoste);
		
		btnVolver = new JButton("Volver");
		btnVolver.addActionListener((ActionListener)this);
		btnVolver.setBounds(10, 261, 108, 41);
		btnVolver.setBackground(new Color(184, 231, 255));
		btnVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/back.png")));
		contentPane.add(btnVolver);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(129, 261, 114, 41);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);		
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener((ActionListener)this);
		btnBorrar.setBackground(new Color(184, 231, 255));
		btnBorrar.setBounds(253, 261, 111, 41);
		btnBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/borrar.png")));
		contentPane.add(btnBorrar);
		
	} // Cierre del metodo componentes
	
	/**
     * Metodo para que los componentes de la ventana realicen una accion concreta.
     *  
     */
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {	
		// Si hace clic en Volver y vuelve a la ventana TablaGastos
		if(evento.getSource()==btnVolver){ 
			
			// Se instancia el controlador TablaGastos
			ControladorTablaProductos controladorEditarProductos=new ControladorTablaProductos();
	  		// Desparece la ventana editar Gastos de Tienda
	  		setVisible(false);
		}
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			// Obtener los datos de cada componente de la ventana
			id=Integer.parseInt(txtID.getText());
			fecha=txtFecha.getText().trim();
			tienda=cbTienda.getSelectedItem().toString().trim();
			producto=txtProducto.getText().trim();
			coste=Double.parseDouble(txtCoste.getText());
			//Sentencia UPDATE
			sql = "UPDATE gastos_producto SET idGastosP="+id+",fechaGP = '"+fecha+"',tienda = '"+tienda+"',producto = '"+producto+"',costeP = '"+coste+"' WHERE idGastosP='"+id+"'";
			// Ejecuta la actualizacion
			BD.actualizar(sql);
			// Muestra un aviso al usuario
			JOptionPane.showMessageDialog(null, "Los datos se ha editado correctamente.");
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece la ventana editar Gastos de Tienda
	  		setVisible(false);
		}
		if(evento.getSource()==btnBorrar){ 
			
			// Obtener el id de la ventana
			id=Integer.parseInt(txtID.getText());
			// Sentencia DELETE
			sql="DELETE FROM gastos_producto WHERE idGastosP=?;";
			BD.borrarRegistro(sql,id); // Elimina el registro
			// Muestra aviso al usuario
			JOptionPane.showMessageDialog(null, "El producto ha sido borrado.");
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece la ventana editar Gastos de Tienda
	  		setVisible(false);
			
		} // Cierre del btnBorrar
		
	} // Cierre del metodo actionPerformed
} // Cierre de la clase