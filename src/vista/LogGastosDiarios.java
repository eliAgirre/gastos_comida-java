package vista;

import java.io.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import controlador.ControladorPrincipal;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class LogGastosDiarios extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JTextArea textArea;
	private JButton btnMenu;
	
	/**
	 * 
     * Constructor sin parametros y contiene el metodo componentes.
     * 
     */
	public LogGastosDiarios(){
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 581, 482); // tama�o
		setTitle("Fichero Gastos Diarios"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/file.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Llama al metodo componentes
		componentes();	
		
		// Obtiene el fichero gastos.log
		//File file = new File("src/archivos/gastos.log");
		File file = new File("archivos/gastos.log");
		
		//Abre el fichero
		try{
		
			String texto="";		
			
			FileReader ficheroR=new FileReader(file);
			BufferedReader bR=new BufferedReader(ficheroR);
			StringBuffer buffer=new StringBuffer();
			texto=bR.readLine();
			
			// Mientras que el fichero contenga texto mostrara
			while(texto!= null){
				
				buffer.append(texto+ "\n"); // Se a�ade una linea de salto
				texto=bR.readLine();

			} // Cierre while
			textArea.append(buffer.toString()); //aparecer el contenido en el textArea
			bR.close();
		
		}
		catch (IOException eoi) {

			JOptionPane.showMessageDialog(null,"El fichero no esta bien.");
		}
		
	} // Cierre constructor

	/**
	 * 
     * Contiene todos los componentes de la ventana Fichero Gastos Diarios.
     *  
     */
	public void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Panel para visualizar y hacer scroll
		scrollPane = new JScrollPane();
		scrollPane.setEnabled(false);
		scrollPane.setBounds(65, 85, 462, 303);
		contentPane.add(scrollPane);
		
		// Label
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(10, 428, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Archivo de gastos de todos los dias:");
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(65, 37, 343, 14);
		contentPane.add(lblAviso);
		
		// Area de texto
		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		textArea.setFont(new java.awt.Font(null, 0, 12));
		
		// Boton
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		btnMenu.setBounds(246, 401, 95, 41);
		contentPane.add(btnMenu);
		
	} // Cierre componentes
	
	/**
     * Metodo para que los componentes de la ventana realicen una accion concreta.
     *  
     */
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {	
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}// Cierre evento btnMenu
		
	} // Cierre del metodo actionPerformed
} // Cierre de la clase