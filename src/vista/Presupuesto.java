package vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;
import java.sql.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import controlador.ControladorPrincipal;
import gestor.BD;

@SuppressWarnings("serial")
public class Presupuesto extends JFrame implements ActionListener{

	// Atributos de la clase
	private JPanel contentPane;
	private JTextField txtFecha;
	private JTextField txtPresu;
	private JButton btnMenu;
	private int id=0;
	private String fecha;
	private double presupuesto;
	private String tipo;
	private JRadioButton rbSemanal;
	private JRadioButton rbMensual;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	
		// Atributos relacionados con la BD
		private static Statement stmt;
		private static ResultSet rs;
		private String sql;
		private boolean tablaVacia;
		
	
	/**
	 * 
     * Constructor sin parametros y contiene el metodo componentes.
     * 
     */
	public Presupuesto(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 300, 370); // tama�o
		setTitle("A�adir presupuesto"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/add.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();
			
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();			
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
		
	} // Cierre del constructor
	
	/**
	 * 
     * Contiene todos los componentes de la ventana Presupuesto.
     *  
     */
	public void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(-1, 316, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Introduzca los datos, por favor.");
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(31, 35, 182, 14);
		contentPane.add(lblAviso);
		
		JLabel lblFecha = new JLabel("Fecha:");
		lblFecha.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFecha.setForeground(new Color(0, 139, 139));
		lblFecha.setBounds(31, 81, 63, 14);
		contentPane.add(lblFecha);
		
		txtFecha = new JTextField();
		txtFecha.setBounds(130, 81, 126, 20);
		contentPane.add(txtFecha);
		txtFecha.setColumns(10);
		
		JLabel lblPresu = new JLabel("Presupuesto:");
		lblPresu.setForeground(new Color(0, 139, 139));
		lblPresu.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPresu.setBounds(31, 133, 89, 14);
		contentPane.add(lblPresu);
		
		txtPresu = new JTextField();
		txtPresu.setColumns(10);
		txtPresu.setBounds(130, 133, 126, 20);
		contentPane.add(txtPresu);
		
		JLabel lblTipo = new JLabel("Tipo:");
		lblTipo.setForeground(new Color(0, 139, 139));
		lblTipo.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTipo.setBounds(31, 196, 89, 14);
		contentPane.add(lblTipo);
		
		rbSemanal = new JRadioButton("Semanal");
		rbSemanal.setSelected(true);
		rbSemanal.addActionListener((ActionListener)this);
		rbSemanal.setBounds(130, 180, 109, 23);
		buttonGroup.add(rbSemanal);
		contentPane.add(rbSemanal);
		
		rbMensual = new JRadioButton("Mensual");
		rbMensual.addActionListener((ActionListener)this);
		rbMensual.setBounds(130, 216, 109, 23);
		buttonGroup.add(rbMensual);
		contentPane.add(rbMensual);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(144, 270, 95, 41);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);	
		
	} // Cierre metodo componentes
	
	/**
     * Metodo para que los componentes de la ventana realicen una accion concreta.
     *  
     */
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Si ha seleccionado radio "Semanal"
		if(evento.getSource()==rbSemanal){
			// Se establece "Semanal" el valor de la variable 
			tipo="Semanal";
			
		}
		// Si ha seleccionado radio "Mensual"
		if(evento.getSource()==rbMensual){
			// Se establece "Mensual" el valor de la variable 
			tipo="Mensual";
			
		}
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			
			try {
				if(txtFecha.getText().equals("")){
					// Se instancia el controlador Principal
			  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
			  		// Desparece la ventana a�adir Gastos de Tienda
			  		setVisible(false);
				}
				else{
					// Sentencia SQL SELECT
					sql = "SELECT * FROM presupuestos;";
					// Comprueba si la tabla esta vacia
					tablaVacia=BD.tablaVacia(stmt,sql);
					// Si la tabla esta vacia
					if(tablaVacia==true){
						id++;
						// Obtener los datos de cada componente de la ventana
						fecha=txtFecha.getText().trim();
						presupuesto=Double.parseDouble(txtPresu.getText().trim());
						// Si el usuario  ha dejado por defecto el radio button
						if(tipo==null){
							// Se establece "Semanal" el valor de la variable
							tipo="Semanal";
						}
						//Sentencia INSERT
						sql = "INSERT INTO presupuestos VALUES ("+id+",'"+fecha+"',"+presupuesto+",'"+tipo+"');";
						// Inserta los datos en la BD
						BD.actualizar(sql);
						// Muestra un aviso para el usuario
						JOptionPane.showMessageDialog(null, "Los datos han sido guardados correctamente.");
						// Se instancia el controlador Principal
				  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
				  		// Desparece la ventana a�adir Gastos de Tienda
				  		setVisible(false);
					}
					else{ // Si la tabla contiene datos				
					
						// Sentencia SQL SELECT
						sql = "SELECT MAX(idPresu) FROM presupuestos;";
						// Ejecuta la consulta
						rs = BD.consulta(stmt, sql);
						// Si hay datos
						if (rs.next()){
							// Se obtiene el ultimo ID desde la BD
							id=rs.getInt(1);
				        }					
						id++;
						// Obtener los datos de cada componente de la ventana
						fecha=txtFecha.getText().trim();
						presupuesto=Double.parseDouble(txtPresu.getText().trim());
						//Sentencia INSERT
						sql = "INSERT INTO presupuestos VALUES ("+id+",'"+fecha+"',"+presupuesto+",'"+tipo+"');";
						// Inserta los datos en la BD
						BD.actualizar(sql);
						// Muestra un aviso para el usuario
						JOptionPane.showMessageDialog(null, "Los datos han sido guardados correctamente.");
						// Se instancia el controlador Principal
				  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
				  		// Desparece la ventana a�adir Gastos de Tienda
				  		setVisible(false);
				  		
					} // Cierre del else
				} // Cierre del else
			}
		  	catch (SQLException e) {
				// Muestra un error
				JOptionPane.showMessageDialog(null, e.getMessage(),"Error ",JOptionPane.ERROR_MESSAGE);
			}	
		}// Cierre evento btnMenu
		
	} // Cierre del metodo actionPerformed
} // Cierre de la clase