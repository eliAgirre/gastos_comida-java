package vista;

import java.awt.Color;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.*;

import controlador.ControladorPrincipal;
import gestor.BD;

@SuppressWarnings("serial")
public class Presupuestos extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private DefaultTableModel modelo;
	private String [] vector={"id", "fecha","cantidad","tipo"}; //cabecera
	private String [][] arrayTabla; //array bidimensional
	private JTable tabla;
	private static String[] datosBD=new String[4];
	private JRadioButton rbSemanal;
	private JRadioButton rbMensual;
	private JButton btnMenu;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JButton btnVer;
	private String tipo;
	
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;		
	
	/**
	 * 
     * Constructor sin parametros y contiene el metodo componentes.
     * 
     */
	public Presupuestos(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 446, 486); // tama�o
		setTitle("Ver presupuestos"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/view.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();	
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();			
			// Sentencia SQL SELECT
			sql = "SELECT * FROM presupuestos;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			// Mientras que haya datos
			while (rs.next()) { 
				
				// Se guardan los datos en array datosBD
				datosBD[0]=Integer.toString(rs.getInt(1));
				datosBD[1]=rs.getString(2);
				datosBD[2]=Double.toString(rs.getDouble(3))+" �";
				datosBD[3]=rs.getString(4);
				// Se a�ade cada fila al modelo de tabla
				modelo.addRow(datosBD);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
		
	} // Cierre constructor
		
	/**
	 * 
     * Contiene todos los componentes de la ventana Presupuestos.
     *  
     */
	public void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Panel para visualizar y hacer scroll
		scrollPane = new JScrollPane();
		scrollPane.setEnabled(false);
		scrollPane.setBounds(10, 80, 420, 303);
		contentPane.add(scrollPane);
		
		// Modelo tabla, contiene cabecera y arrayBidimensional para los datos
		modelo = new DefaultTableModel(arrayTabla,vector);
		
		// Se le pasa a JTable el modelo de tabla
		tabla = new JTable(modelo);
		tabla.setEnabled(false);
		scrollPane.setViewportView(tabla);// Se a�ade la tabla al panel scroll
		
		// Label + radio buttons + botones
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 432, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Seleccione una opcion a ver:");
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(10, 35, 182, 14);
		contentPane.add(lblAviso);
		
		rbSemanal = new JRadioButton("Semanal");
		rbSemanal.setSelected(true);
		rbSemanal.addActionListener((ActionListener)this);
		rbSemanal.setBounds(180, 20, 109, 23);
		buttonGroup.add(rbSemanal);
		contentPane.add(rbSemanal);
		
		rbMensual = new JRadioButton("Mensual");
		rbMensual.addActionListener((ActionListener)this);
		rbMensual.setBounds(180, 48, 109, 23);
		buttonGroup.add(rbMensual);
		contentPane.add(rbMensual);
		
		btnVer = new JButton("Ver");
		btnVer.addActionListener((ActionListener)this);
		btnVer.setBounds(295, 18, 109, 49);
		btnVer.setBackground(new Color(184, 231, 255));
		btnVer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/view.png")));
		contentPane.add(btnVer);
		
		// Boton menu
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(169, 413, 106, 33);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);
		
	} // Cierre del metodo componentes
	
	/**
     * Metodo para limpiar los datos de la tabla.
     *  
     */
	public void limpiarTabla() {
		
		modelo.setRowCount(0);
	} // Cierre de limpiarTabla
	
	/**
     * Metodo para que los componentes de la ventana realicen una accion concreta.
     *  
     */
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {	
		// Si ha seleccionado radio "Semanal"
		if(evento.getSource()==rbSemanal){
			// Limpia la tabla
			limpiarTabla();
			// Se establece "Semanal" el valor de la variable 
			tipo="Semanal";
			
		}
		// Si ha seleccionado radio "Mensual"
		if(evento.getSource()==rbMensual){
			// Limpia la tabla
			limpiarTabla();
			// Se establece "Mensual" el valor de la variable 
			tipo="Mensual";
			
		}
		// Si hace clic en Ver podra ver los presupuestos que haya seleccionado
		if(evento.getSource()==btnVer){ 
			try {
				// Sentencia SQL SELECT
				sql = "SELECT * FROM presupuestos WHERE tipo='"+tipo+"';";
				// Hace la consulta y devuelve el resultado
				rs = BD.consulta(stmt,sql);
				// Mientras que haya datos
				while (rs.next()) { 
					
					// Se guardan los datos en array datosBD
					datosBD[0]=Integer.toString(rs.getInt(1));
					datosBD[1]=rs.getString(2);
					datosBD[2]=Double.toString(rs.getDouble(3))+" �";
					datosBD[3]=rs.getString(4);
					// Se a�ade cada fila al modelo de tabla
					modelo.addRow(datosBD);
					
				} // Cierre de while
				
				rs.close(); //Cierre de la consulta
			}
			catch (SQLException e) {
				// Muestra error SQL
				JOptionPane.showMessageDialog(null, e.getMessage());
				
			} // Cierre excepcion
		}
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}// Cierre evento btnMenu
		
	} // Cierre del metodo actionPerformed
} // Cierre de la clase