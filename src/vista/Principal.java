package vista;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

import controlador.*;

/**
 * Extiende metodos de JFrame implementando ActionListener teniendo los atributos de tipo JButton para acceder a otras ventanas y otros componentes m�s.
 * 
 *  @version 2.0
 * 
 */
@SuppressWarnings("serial")
public class Principal extends JFrame implements ActionListener{
	
	//Atributos de la clase
	private JPanel contentPane;
	private JMenu mnFichero;
	private static String version="2.0";
	
		// items del menu
		private JMenuItem itemPresu;
		private JMenuItem itemProductoA;
		private JMenuItem itemTienda;
		private JMenuItem itemGastoE;
		private JMenuItem itemProducto;
		private JMenuItem itemPresupuestos;
		private JMenuItem itemGastosDia;
		private JMenuItem itemGastosFechas;
		private JMenuItem itemSemanal;
		private JMenuItem itemMensual;
		private JMenuItem itemA_semanal;
		private JMenuItem itemA_mensual;
		private JMenuItem itemGastosDiarios;
		private JMenuItem itemGastosMensuales;
		private JMenuItem itemAhorroMensual;
		
		// accesos directos - botones
		private JButton btnTienda;
		private JButton btnProductos;
		private JButton btnGastos;
		private JButton btnDiario;
		private JButton btnSemanal;
		private JButton btnAhorro;
		
	
	/**
	 * 
     * Constructor sin parametros y contiene el metodo componentes.
     * 
     */
	public Principal(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 583, 326); // tama�o
		setTitle("Gastos de comida"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/coins.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // La equis de la ventana detiene la aplicacion
		
		// Llama al metodo componentes
		componentes();
		
	} // Cierre del constructor
	
	/**
	 * 
     * Contiene todos los componentes de la ventana Principal.
     *  
     */
	public void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//contenedor de menu bar
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 581, 21);
		contentPane.add(menuBar);
		
		// menu + items del menu
		JMenu mnAnadir = new JMenu("A�adir"); 
		menuBar.add(mnAnadir);
		
		itemPresu = new JMenuItem("Presupuesto");
		itemPresu.addActionListener((ActionListener)this);
		mnAnadir.add(itemPresu);
		
		JMenu mnGastoA = new JMenu("Gasto");
		mnAnadir.add(mnGastoA);
		
		itemProductoA = new JMenuItem("Producto");
		itemProductoA.addActionListener((ActionListener)this);
		mnGastoA.add(itemProductoA);
		
		itemTienda = new JMenuItem("Tienda");
		itemTienda.addActionListener((ActionListener)this);
		mnGastoA.add(itemTienda);
		
		
		JMenu mnEditar = new JMenu("Editar");
		menuBar.add(mnEditar);
		
		itemGastoE = new JMenuItem("Gasto de tienda");
		itemGastoE.addActionListener((ActionListener)this);
		mnEditar.add(itemGastoE);
		
		itemProducto = new JMenuItem("Productos");
		itemProducto.addActionListener((ActionListener)this);
		mnEditar.add(itemProducto);
		
		
		
		JMenu mnVer = new JMenu("Ver");
		menuBar.add(mnVer);
		
		itemPresupuestos = new JMenuItem("Presupuestos");
		itemPresupuestos.addActionListener((ActionListener)this);
		mnVer.add(itemPresupuestos);		
		
		itemGastosDia = new JMenuItem("Gastos diarios");
		itemGastosDia.addActionListener((ActionListener)this);
		mnVer.add(itemGastosDia);
		
		itemGastosFechas = new JMenuItem("Gastos concretos");
		itemGastosFechas.addActionListener((ActionListener)this);
		mnVer.add(itemGastosFechas);
		
		
		
		JMenu mnCalculo = new JMenu("C�lculo");
		menuBar.add(mnCalculo);
		
		itemSemanal = new JMenuItem("Semanal");
		itemSemanal.addActionListener((ActionListener)this);
		mnCalculo.add(itemSemanal);
		
		itemMensual = new JMenuItem("Mensual");
		itemMensual.addActionListener((ActionListener)this);
		mnCalculo.add(itemMensual);	
		
		JMenu mnAhorro = new JMenu("Ahorro");
		menuBar.add(mnAhorro);
		
		itemA_semanal = new JMenuItem("Semanal");
		itemA_semanal.addActionListener((ActionListener)this);
		mnAhorro.add(itemA_semanal);
		
		itemA_mensual = new JMenuItem("Mensual");
		itemA_mensual.addActionListener((ActionListener)this);
		mnAhorro.add(itemA_mensual);
		
		
		
		mnFichero = new JMenu("Fichero");
		//mnFichero.setEnabled(false);
		menuBar.add(mnFichero);
		
		itemGastosDiarios = new JMenuItem("Gastos diarios");
		itemGastosDiarios.addActionListener((ActionListener)this);
		mnFichero.add(itemGastosDiarios);
		
		itemGastosMensuales = new JMenuItem("Gastos mensuales");
		itemGastosMensuales.addActionListener((ActionListener)this);
		mnFichero.add(itemGastosMensuales);
		
		itemAhorroMensual = new JMenuItem("Ahorro mensual");
		itemAhorroMensual.addActionListener((ActionListener)this);
		mnFichero.add(itemAhorroMensual);
		
		
		// Accesos directos - botones
		btnTienda = new JButton("Tienda");
		btnTienda.addActionListener((ActionListener)this);
		btnTienda.setBackground(new Color(184, 231, 255));
		btnTienda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/add.png")));
		btnTienda.setBounds(10, 75, 143, 76);
		contentPane.add(btnTienda);
		
		btnProductos = new JButton("Productos");
		btnProductos.addActionListener((ActionListener)this);
		btnProductos.setBackground(new Color(184, 231, 255));
		btnProductos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/add.png")));
		btnProductos.setBounds(206, 75, 143, 76);
		contentPane.add(btnProductos);
		
		btnGastos = new JButton("Gastos");
		btnGastos.addActionListener((ActionListener)this);
		btnGastos.setBackground(new Color(184, 231, 255));
		btnGastos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/edit.png")));
		btnGastos.setBounds(404, 75, 152, 76);
		contentPane.add(btnGastos);
		
		btnDiario = new JButton("Diario");
		btnDiario.addActionListener((ActionListener)this);
		btnDiario.setBackground(new Color(184, 231, 255));
		btnDiario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/view.png")));
		btnDiario.setBounds(10, 194, 143, 76);
		contentPane.add(btnDiario);
		
		btnSemanal = new JButton("Semanal");
		btnSemanal.addActionListener((ActionListener)this);
		btnSemanal.setBackground(new Color(184, 231, 255));
		btnSemanal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/calc.png")));
		btnSemanal.setBounds(206, 194, 143, 76);
		contentPane.add(btnSemanal);
		
		btnAhorro = new JButton("Ahorro");
		btnAhorro.addActionListener((ActionListener)this);
		btnAhorro.setBackground(new Color(184, 231, 255));
		btnAhorro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/coins.png")));
		btnAhorro.setBounds(404, 194, 152, 76);
		contentPane.add(btnAhorro);

		
		JLabel lblVersion = new JLabel("Version: "+version);
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 281, 95, 14);
		contentPane.add(lblVersion);
	
	} // Cierre del metodo componentes
	
	/**
     * Metodo que devuelve la version de la app para mostrar en labels.
     *  
     */
	public static String version(){
		return version;
	}
	
	/**
     * Metodo para que los componentes de la ventana realicen una accion concreta.
     *  
     */
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Si hace clic en menu Presupuesto podra a�adir un Presupuesto
		if(evento.getSource()==itemPresu ){ 
			// Se instancia el controlador Tienda
	  		ControladorPresupuesto controladorTienda=new ControladorPresupuesto();
	  		// Deaparece la ventana principal
	  		setVisible(false);
		}
		// Si hace clic en boton Tienda o menu Tienda podra a�adir "Gastos de Tienda"
		if(evento.getSource()==btnTienda || evento.getSource()==itemTienda ){ 
			// Se instancia el controlador Tienda
	  		ControladorTienda controladorTienda=new ControladorTienda();
	  		// Deaparece la ventana principal
	  		setVisible(false);
		}
		// Si hace clic en boton Productos o menu Producto podra a�adir "Gastos de Producto"
		if(evento.getSource()==btnProductos || evento.getSource()==itemProductoA ){ 
			// Se instancia el controlador Producto
	  		ControladorProducto controladorProducto=new ControladorProducto();
	  		// Deaparece la ventana principal
	  		setVisible(false);
		}
		// Si hace clic en boton Diario o menu Gastos diarios podra ver "Gastos de Diarios"
		if(evento.getSource()==btnDiario || evento.getSource()==itemGastosDia ){ 
			// Se instancia el controlador Diario
	  		ControladorDiario controladorDiario=new ControladorDiario();
	  		// Deaparece la ventana principal
	  		setVisible(false);
		}
		// Si hace clic en boton Gastos o menu Gastos tienda podra editar "Gastos de Tienda"
		if(evento.getSource()==btnGastos || evento.getSource()==itemGastoE ){ 
			// Se instancia el controlador Diario
	  		ControladorTablaGastos controladorEditarGastos=new ControladorTablaGastos();
	  		// Deaparece la ventana principal
	  		setVisible(false);
		}
		// Si hace clic en menu Gastos producto podra editar "Gastos de los productos"
		if(evento.getSource()==itemProducto ){ 
			// Se instancia el controlador Diario
	  		ControladorTablaProductos controladorEditarProductos=new ControladorTablaProductos();
	  		// Deaparece la ventana principal
	  		setVisible(false);
		}
		// Si hace clic en menu "Presupuestos" podra ver todos los presupuestos
		if(evento.getSource()==itemPresupuestos ){ 
			// Se instancia el controlador Producto
	  		ControladorPresupuestos controladorPresupuestos=new ControladorPresupuestos();
	  		// Deaparece la ventana principal
	  		setVisible(false);
		}
		// Si hace clic en menu "Gastos concretos" podra ver los gastos entre fechas
		if(evento.getSource()==itemGastosFechas){
			
			String inputFecha1;
			String inputFecha2;
			String nl = System.getProperty("line.separator"); // Salto de linea
			String inputOpcion=JOptionPane.showInputDialog("Introduce un numero (1 o 2):"+nl+" "+nl+"1. Ver gastos de un dia"+nl+"2. Gastos entre 2 fechas"+nl+" ");
			// Si el usuario ha pulsado "Cancelar
			if(inputOpcion==null){ 
				// Muestra la ventana principal
				setVisible(true);
			}
			else{ // Si el usuario ha pulsado "Aceptar" tiene que introducir fecha o fechas
				// Si ha introducido 1
				if(inputOpcion.equals("1")){
					// Escribe una fecha
					inputFecha1=JOptionPane.showInputDialog("Introduce la fecha:");
					// Se instancia el controlador Fechas
			  		ControladorFechas controladorFechas=new ControladorFechas(inputFecha1);
			  		// Deaparece la ventana principal
			  		setVisible(false);
				}
				// Si ha introducido 2
				if(inputOpcion.equals("2")){
					// Escribe una fecha
					inputFecha1=JOptionPane.showInputDialog("Introduce una fecha: "+nl+" "+nl+"Tiene que ser a partir de una fecha"+nl+" ");
					inputFecha2=JOptionPane.showInputDialog("Introduce otra fecha: "+nl+" "+nl+"Hasta una fecha limite"+nl+" ");
					// Se instancia el controlador Fechas
			  		ControladorFechas controladorFechas=new ControladorFechas(inputFecha1,inputFecha2);
			  		// Deaparece la ventana principal
			  		setVisible(false);
				}
				
			} // Cierre del else
		}
		// Si hace clic en el boton "Semanal" o menu Calculo Semanal
		if(evento.getSource()==btnSemanal || evento.getSource()==itemSemanal ){ 
			// Se instancia el controlador Semanal
	  		ControladorSemanal controladorSemanal=new ControladorSemanal();
	  		// Deaparece la ventana principal
	  		setVisible(false);
		}
		// Si hace clic en menu Calculo Mensual
		if(evento.getSource()==itemMensual ){ 
			// Se instancia el controlador Mensual
	  		ControladorMensual controladorMensual=new ControladorMensual();
	  		// Deaparece la ventana principal
	  		setVisible(false);
		}
		// Si hace clic en el boton "Ahorro" o en menu Ahorro Semanal
		if(evento.getSource()==btnAhorro || evento.getSource()==itemA_semanal){ 
			// Se instancia el controlador Ahorro
	  		ControladorAhorro controladorAhorro=new ControladorAhorro();
	  		// Deaparece la ventana principal
	  		setVisible(false);
		}
		// Si hace clic en menu Ahorro Mensual
		if(evento.getSource()==itemA_mensual){ 
			// Se instancia el controlador Ahorro
	  		ControladorAhorroMensual controladorAhorroMensual=new ControladorAhorroMensual();
	  		// Deaparece la ventana principal
	  		setVisible(false);
		}
		// Si hace clic en menu Fichero => Gastos Diarios
		if(evento.getSource()==itemGastosDiarios){ 
			// Se instancia el controlador Ahorro
	  		ControladorLogGastosDiarios controladorLogGastosDiarios=new ControladorLogGastosDiarios();
	  		// Deaparece la ventana principal
	  		setVisible(false);
		}
		// Si hace clic en menu Fichero => Gastos Mensuales
		if(evento.getSource()==itemGastosMensuales){ 
			// Se instancia el controlador Ahorro
	  		ControladorLogGastosMensuales controladorLogGastosMensuales=new ControladorLogGastosMensuales();
	  		// Deaparece la ventana principal
	  		setVisible(false);
		}
		// Si hace clic en menu Fichero => Ahorro Mensual
		if(evento.getSource()==itemAhorroMensual){ 
			// Se instancia el controlador Ahorro
	  		ControladorLogAhorroMensual controladorLogAhorroMensual=new ControladorLogAhorroMensual();
	  		// Deaparece la ventana principal
	  		setVisible(false);
		}
	} // Cierre del metodo actionPerformed
} // Cierre de la clase