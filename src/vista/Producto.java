package vista;

import java.awt.*;
import java.awt.event.*;
import java.sql.*;

import javax.swing.*;
import javax.swing.border.*;

import controlador.ControladorPrincipal;
import gestor.BD;
import gestor.GastosProductos;
import modelo.Gastos_P;
import modelo.Tiendas;

@SuppressWarnings("serial")
public class Producto extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private Tiendas tiendas;
	private JTextField txtFecha;
	private JComboBox<Tiendas> cbTienda;
	private int idTienda;
	private String nombreTienda;
	private JTextField txtProducto;
	private JTextField txtCoste;
	private JButton btnOtroMas;
	private JButton btnMenu;
	private int id;
	private String fecha;
	private String tienda;
	private String producto;
	private double coste;
	private int cont=0;
	private GastosProductos gestorGastosP=new GastosProductos(); //arraylist
	private Gastos_P gastosProducto;

		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
		private boolean tablaVacia;
	
	/**
	 * 
     * Constructor sin parametros y contiene el metodo componentes.
     * 
     */
	public Producto(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 300, 377); // tama�o
		setTitle("Gastos de producto"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/add.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();	
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();			
			// Sentencia SQL SELECT
			sql = "SELECT * FROM tiendas;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos, id + nombreTienda
				idTienda=rs.getInt("id");
				nombreTienda=rs.getString("nombre");
				
				// Se instancia la clase tienda
				tiendas = new Tiendas (idTienda,nombreTienda); 
				
				//Se a�ade cada tienda al JComboBox
				cbTienda.addItem(tiendas);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion	
		
	} // Cierre del constructor
	
	/**
	 * 
     * Contiene todos los componentes de la ventana Producto.
     *  
     */
	public void componentes (){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(-1, 325, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Introduzca los datos, por favor.");
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(31, 35, 182, 14);
		contentPane.add(lblAviso);
		
		JLabel lblFecha = new JLabel("Fecha:");
		lblFecha.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFecha.setForeground(new Color(0, 139, 139));
		lblFecha.setBounds(48, 81, 46, 14);
		contentPane.add(lblFecha);
		
		txtFecha = new JTextField();
		txtFecha.setBounds(130, 81, 126, 20);
		contentPane.add(txtFecha);
		txtFecha.setColumns(10);
		
		JLabel lblTienda = new JLabel("Tienda:");
		lblTienda.setForeground(new Color(0, 139, 139));
		lblTienda.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTienda.setBounds(48, 125, 46, 14);
		contentPane.add(lblTienda);
		
		cbTienda = new JComboBox<Tiendas>(); // Muestra la lista de la clase Tiendas
		cbTienda.setBounds(130, 125, 126, 20);
		contentPane.add(cbTienda);
		
		JLabel lblProducto = new JLabel("Producto:");
		lblProducto.setForeground(new Color(0, 139, 139));
		lblProducto.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblProducto.setBounds(48, 171, 66, 14);
		contentPane.add(lblProducto);
		
		txtProducto = new JTextField();
		txtProducto.setColumns(10);
		txtProducto.setBounds(130, 171, 126, 20);
		contentPane.add(txtProducto);
		
		JLabel lblCoste = new JLabel("Coste:");
		lblCoste.setForeground(new Color(0, 139, 139));
		lblCoste.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCoste.setBounds(48, 221, 66, 14);
		contentPane.add(lblCoste);
		
		txtCoste = new JTextField();
		txtCoste.setColumns(10);
		txtCoste.setBounds(130, 218, 126, 20);
		contentPane.add(txtCoste);
		
		btnOtroMas = new JButton("Otro");
		btnOtroMas.addActionListener((ActionListener)this);
		btnOtroMas.setBackground(new Color(184, 231, 255));
		btnOtroMas.setBounds(48, 272, 95, 41);
		btnOtroMas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/add.png")));
		contentPane.add(btnOtroMas);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setBounds(161, 272, 95, 41);
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);
		
	} // Cierre del metodo componentes
	
	/**
	 * 
     * Limpia los datos de los JTextFields y JComboBox.
     *  
     */
	public void limpiar(){
		txtFecha.setText("");
		cbTienda.setSelectedIndex(0);
		txtProducto.setText("");
		txtCoste.setText("");
	}
	
	/**
     * Metodo para que los componentes de la ventana realicen una accion concreta.
     *  
     */
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Si hace clic en Otro, se van a�adiendo registros al array hasta que da al Menu
		if(evento.getSource()==btnOtroMas){
			
			try {				
				// Sentencia SQL SELECT
				sql = "SELECT * FROM gastos_producto;";
				// Comprueba si la tabla esta vacia
				tablaVacia=BD.tablaVacia(stmt,sql);
				// Se a�aden los datos de cada componente al arraylist
				if(tablaVacia==true){
					id++;
					// Obtener los datos de cada componente de la ventana
					fecha=txtFecha.getText().trim();
					tienda=cbTienda.getSelectedItem().toString().trim();
					producto=txtProducto.getText().trim();
					coste=Double.parseDouble(txtCoste.getText());
					//Se crea un nuevo objeto de gastoTiendas
					gastosProducto=new Gastos_P(id,fecha,tienda,producto,coste);					
					// Se a�ade cada objeto al arraylist
					gestorGastosP.anadir(gastosProducto);
					// Se limpian los componentes
					limpiar();
				}
				else{
					cont++;
					// Cuando cont sea igual a 1 se obtiene la ID de la BD
					if(cont==1){
						// Se obtiene el ultimo ID desde la BD
						id=gestorGastosP.ultimoID();
					}					
					id++;
					// Obtener los datos de cada componente de la ventana
					fecha=txtFecha.getText().trim();
					tienda=cbTienda.getSelectedItem().toString().trim();
					producto=txtProducto.getText().trim();
					coste=Double.parseDouble(txtCoste.getText());
					//Se crea un nuevo objeto de gastoTiendas
					gastosProducto=new Gastos_P(id,fecha,tienda,producto,coste);					
					// Se a�ade cada objeto al arraylist
					gestorGastosP.anadir(gastosProducto);
					// Se limpian los componentes
					limpiar();
					
				} // Cierre del else				
			}
			catch (SQLException e) {
				// Muestra un error
				JOptionPane.showMessageDialog(null, e.getMessage(),"Error ",JOptionPane.ERROR_MESSAGE);
			}
		}		
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){ 	
			
			try {
				if(txtFecha.getText().equals("")){
					// Se instancia el controlador Principal
			  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
			  		// Desparece la ventana a�adir Gastos de Tienda
			  		setVisible(false);
				}
				else{
					cont++;
					// Cuando cont sea igual a 1 se obtiene la ID de la BD
					if(cont==1){
						// Se obtiene el ultimo ID desde la BD
						id=gestorGastosP.ultimoID();
					}
					id++;
					// Obtener los datos de cada componente de la ventana
					fecha=txtFecha.getText().trim();
					tienda=cbTienda.getSelectedItem().toString().trim();
					producto=txtProducto.getText().trim();
					coste=Double.parseDouble(txtCoste.getText());
					//Se crea un nuevo objeto de gastoTiendas
					gastosProducto=new Gastos_P(id,fecha,tienda,producto,coste);					
					// Se a�ade cada objeto al arraylist
					gestorGastosP.anadir(gastosProducto);
					//JOptionPane.showMessageDialog(null,gestorGastosT.listar());
					// Inserta los datos del array en la BD
					gestorGastosP.insertarBD();
					// Muestra un aviso para el usuario
					JOptionPane.showMessageDialog(null, "Los datos han sido guardados correctamente.");
					// Se instancia el controlador Principal
			  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
			  		// Desparece la ventana a�adir Gastos de Tienda
			  		setVisible(false);
				} // Cierre del else
			}
		  	catch (SQLException e) {
				// Muestra un error
				JOptionPane.showMessageDialog(null, e.getMessage(),"Error ",JOptionPane.ERROR_MESSAGE);
			}		
		}// Cierre evento btnMenu	
		
	} // Cierre del metodo actionPerformed
} // Cierre de la clase