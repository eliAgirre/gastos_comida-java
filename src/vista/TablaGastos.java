package vista;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.*;

import controlador.ControladorEditarGastos;
import controlador.ControladorPrincipal;
import gestor.BD;

@SuppressWarnings("serial")
public class TablaGastos extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private DefaultTableModel modelo;
	private String [] vector={"id", "fecha","tienda","coste","forma de pago", "tarjeta"}; //cabecera
	private String [][] arrayTabla; //array bidimensional
	private JTable tabla;
	private static String[] datosBD=new String[6];
	private JButton btnMenu;
	private String id;
	
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
		
	/**
	 * 
     * Constructor sin parametros y contiene el metodo componentes haciendo una consulta a la BD.
     * 
     */
	public TablaGastos(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 579, 444); // tama�o
		setTitle("Editar gastos de tienda"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/edit.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();	
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();			
			// Sentencia SQL SELECT
			sql = "SELECT * FROM gastos_tienda;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			// Mientras que haya datos
			while (rs.next()) { 
				
				// Se guardan los datos en array datosBD
				datosBD[0]=Integer.toString(rs.getInt(1));
				datosBD[1]=rs.getString(2);
				datosBD[2]=rs.getString(3);
				datosBD[3]=Double.toString(rs.getDouble(4))+" �";
				datosBD[4]=rs.getString(5);
				datosBD[5]=rs.getString(6);
				// Se a�ade cada fila al modelo de tabla
				modelo.addRow(datosBD);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion
		
	} // Cierre constructor
	
	/**
	 * 
     * Contiene todos los componentes de la ventana Diario.
     *  
     */
	public void componentes(){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Panel para visualizar y hacer scroll
		scrollPane = new JScrollPane();
		scrollPane.setEnabled(false);
		scrollPane.setBounds(10, 55, 553, 303);
		contentPane.add(scrollPane);
		
		// Modelo tabla, contiene cabecera y arrayBidimensional para los datos
		modelo = new DefaultTableModel(arrayTabla,vector);
		
		// Se le pasa a JTable el modelo de tabla
		tabla = new JTable(modelo);
		// Se a�ade un listener a la tabla
		tabla.addMouseListener(new MouseAdapter() {
			// Cuando el raton seleccione un dato de una fila muestra un aviso
		    @SuppressWarnings("unused")
			public void mouseClicked(MouseEvent evento) {
		    	
		    	String nl = System.getProperty("line.separator"); // Salto de linea
		        int fila = tabla.rowAtPoint(evento.getPoint());
		        int columna = 0;
		        if ((fila > -1) && (columna > -1))
	        	id=(String)modelo.getValueAt(fila,columna);
		        String fecha=(String) modelo.getValueAt(fila,1);
		        String tienda=(String) modelo.getValueAt(fila,2);
		        String coste=(String) modelo.getValueAt(fila,3);
		        String formaPago=(String) modelo.getValueAt(fila,4);
		        String tarjeta=(String) modelo.getValueAt(fila,5);
		        JOptionPane.showMessageDialog(null, "Has elegido dia ("+fecha+"): "+nl+" "+nl+"     "+tienda+": "+coste+nl+" ");
		        ControladorEditarGastos editarGastos = new ControladorEditarGastos(id,fecha,tienda,coste,formaPago,tarjeta);
				setVisible(false);	
				 
		    } // Cierre del mouseClicked
		});	
		tabla.setEnabled(false);
		scrollPane.setViewportView(tabla);// Se a�ade la tabla al panel scroll
		
		// Boton menu
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(238, 374, 106, 33);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);
		
		// Label
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(0, 393, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblSeleccioneUnGasto = new JLabel("Seleccione un gasto a editar, por favor.");
		lblSeleccioneUnGasto.setHorizontalAlignment(SwingConstants.LEFT);
		lblSeleccioneUnGasto.setBounds(10, 21, 334, 14);
		contentPane.add(lblSeleccioneUnGasto);
		
	} // Cierre del metodo componentes
	
	/**
     * Metodo para que los componentes de la ventana realicen una accion concreta.
     *  
     */
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {	
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			// Se instancia el controlador Principal
	  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
	  		// Desparece esta ventana
	  		setVisible(false);
		}// Cierre evento btnMenu
		
	} // Cierre del metodo actionPerformed
} // Cierre de la clase