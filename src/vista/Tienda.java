package vista;

import vista.Principal;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;

import controlador.ControladorPrincipal;
import gestor.BD;
import gestor.GastosTiendas;
import modelo.Gastos_T;
import modelo.Tiendas;

@SuppressWarnings("serial")
public class Tienda extends JFrame implements ActionListener{
	
	// Atributos de la clase
	private JPanel contentPane;
	private JTextField txtFecha;
	private JLabel lblTienda;
	private JComboBox<Tiendas> cbTienda;
	private JTextField txtCoste;
	private JButton btnOtroMas;
	private JButton btnMenu;
	private Tiendas tiendas;
	private int idTienda;
	private String nombreTienda;
	private GastosTiendas gestorGastosT=new GastosTiendas(); //arraylist
	private int id=0;
	private String fecha;
	private String tienda;
	private double coste;
	private Gastos_T gastoTiendas;
	private int cont=0;
	private JRadioButton rbEfectivo;
	private JRadioButton rbTarjeta;
	private final ButtonGroup pagoGroup = new ButtonGroup();
	private JRadioButton rbAmaia;
	private JRadioButton rbPeio;
	private final ButtonGroup tarjetaGroup = new ButtonGroup();
	private String formaPago;
	private String nombreTarjeta;
		
		// Atributos relacionados con BD
		private static Statement stmt;
		private String sql;
		private static ResultSet rs;
		private boolean tablaVacia;		
	
	/**
	 * 
     * Constructor sin parametros y contiene el metodo componentes.
     * 
     */
	public Tienda(){
		
		// Caracteristicas de la ventana
		setResizable(false); // no reestablece el tama�o
		setBounds(100, 100, 370, 482); // tama�o
		setTitle("Gastos de tienda"); // titulo
		setIconImage(new ImageIcon(getClass().getResource("Img/add.png")).getImage()); //logo
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // La equis de la ventana no cierra la ventana
		
		// Se hace la conexion con la BD
		try {
			// Llama al metodo componentes
			componentes();	
			// Conexion BD
			BD.conectar();
			// Establece conexion devolviendo un resultado
			stmt=BD.conexion();			
			// Sentencia SQL SELECT
			sql = "SELECT * FROM tiendas;";
			// Hace la consulta y devuelve el resultado
			rs = BD.consulta(stmt,sql);
			//Mientras que haya datos
			while (rs.next()) { 
				
				//Obtienen los datos, id + nombreTienda
				idTienda=rs.getInt("id");
				nombreTienda=rs.getString("nombre");
				
				// Se instancia la clase tienda
				tiendas = new Tiendas (idTienda,nombreTienda); 
				
				//Se a�ade cada tienda al JComboBox
				cbTienda.addItem(tiendas);
				
			} // Cierre de while
			
			rs.close(); //Cierre de la consulta
		}
		catch (SQLException e) {
			// Muestra error SQL
			JOptionPane.showMessageDialog(null, e.getMessage());
			
		} // Cierre excepcion		
	} // Cierre del constructor
	
	/**
	 * 
     * Contiene todos los componentes de la ventana Tienda.
     *  
     */
	public void componentes (){
		
		// Layout
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Labels + textFields + comboBox + buttons
		JLabel lblVersion = new JLabel("Version: "+Principal.version());
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(10, 415, 95, 14);
		contentPane.add(lblVersion);
		
		JLabel lblAviso = new JLabel("Introduzca los datos, por favor.");
		lblAviso.setHorizontalAlignment(SwingConstants.LEFT);
		lblAviso.setBounds(31, 35, 182, 14);
		contentPane.add(lblAviso);
		
		JLabel lblFecha = new JLabel("Fecha:");
		lblFecha.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFecha.setForeground(new Color(0, 139, 139));
		lblFecha.setBounds(48, 81, 46, 14);
		contentPane.add(lblFecha);
		
		txtFecha = new JTextField();
		txtFecha.setBounds(159, 81, 126, 20);
		contentPane.add(txtFecha);
		txtFecha.setColumns(10);
		
		lblTienda = new JLabel("Tienda:");
		lblTienda.setForeground(new Color(0, 139, 139));
		lblTienda.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTienda.setBounds(48, 125, 46, 14);
		contentPane.add(lblTienda);
		
		cbTienda = new JComboBox<Tiendas>(); // Muestra la lista de la clase Tiendas
		cbTienda.setBounds(159, 125, 126, 20);
		contentPane.add(cbTienda);
		
		JLabel lblCoste = new JLabel("Coste:");
		lblCoste.setForeground(new Color(0, 139, 139));
		lblCoste.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCoste.setBounds(48, 175, 46, 14);
		contentPane.add(lblCoste);
		
		txtCoste = new JTextField();
		txtCoste.setColumns(10);
		txtCoste.setBounds(159, 175, 126, 20);
		contentPane.add(txtCoste);
		
		btnOtroMas = new JButton("Otro");
		btnOtroMas.addActionListener((ActionListener)this);
		btnOtroMas.setBounds(41, 356, 115, 41);
		btnOtroMas.setBackground(new Color(184, 231, 255));
		btnOtroMas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/add.png")));
		contentPane.add(btnOtroMas);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener((ActionListener)this);
		btnMenu.setBounds(170, 356, 115, 41);
		btnMenu.setBackground(new Color(184, 231, 255));
		btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/Img/home.png")));
		contentPane.add(btnMenu);		
		
		JLabel lblFormaDePago = new JLabel("Forma de pago:");
		lblFormaDePago.setForeground(new Color(0, 139, 139));
		lblFormaDePago.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFormaDePago.setBounds(48, 227, 104, 14);
		contentPane.add(lblFormaDePago);
		
		rbEfectivo = new JRadioButton("Efectivo");		
		rbEfectivo.setSelected(true);
		rbEfectivo.addActionListener((ActionListener)this);
		rbEfectivo.setBounds(159, 212, 109, 23);
		pagoGroup.add(rbEfectivo);
		contentPane.add(rbEfectivo);
		
		rbTarjeta = new JRadioButton("Tarjeta");		
		rbTarjeta.addActionListener((ActionListener)this);
		rbTarjeta.setBounds(159, 239, 109, 23);
		pagoGroup.add(rbTarjeta);
		contentPane.add(rbTarjeta);
		
		JLabel lblTarjeta = new JLabel("Tarjeta:");
		lblTarjeta.setForeground(new Color(0, 139, 139));
		lblTarjeta.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTarjeta.setBounds(48, 288, 104, 14);
		contentPane.add(lblTarjeta);
		
		rbAmaia = new JRadioButton("Amaia");
		rbAmaia.setSelected(true);
		rbAmaia.setEnabled(false);
		rbAmaia.addActionListener((ActionListener)this);
		rbAmaia.setBounds(159, 279, 109, 23);
		tarjetaGroup.add(rbAmaia);
		contentPane.add(rbAmaia);
		
		rbPeio = new JRadioButton("Peio");		
		rbPeio.setEnabled(false);
		rbPeio.addActionListener((ActionListener)this);
		rbPeio.setBounds(159, 305, 109, 23);
		tarjetaGroup.add(rbPeio);
		contentPane.add(rbPeio);
		
		
		
	} // Cierre del metodo componentes
	
	/**
	 * 
     * Limpia los datos de los JTextFields y JComboBox.
     *  
     */
	public void limpiar(){
		txtFecha.setText("");
		cbTienda.setSelectedIndex(0);
		txtCoste.setText("");
	}
	
	/**
     * Obtiene la fecha de sistema.
     * 
     * @return ts Devuelve un Timestamp.
     * 
     */
	private static Timestamp obtenerFechaSistema(){
		
		//creamos el objeto calendar y obtenemos la hora del sistema
		Calendar cal = Calendar.getInstance();
		
		//Se convierte el objeto calendar a timestamp
		Timestamp ts = new Timestamp(cal.getTimeInMillis()); 
		
		return ts;
		
	} //Cierre del m�todo obtenerHoraSistema
	
	/**
	 * 
     * Guarda los datos en un fichero log.
     *  
     */
	public static void gastosLog(String evento, int idGastos, String fecha_compra, String tienda, double gasto){
		
		try {
			
			//FileWriter ficheroW=new FileWriter("src/archivos/gastos.log",true);
			FileWriter ficheroW=new FileWriter("archivos/gastos.log",true);
			BufferedWriter bW=new BufferedWriter(ficheroW);
			
			Timestamp hora=obtenerFechaSistema();
			
			String datos=evento+"   "+hora +"   "+Integer.toString(idGastos)+"   "+fecha_compra+ "    "+tienda+ "      "+Double.toString(gasto);

			bW.write(datos);
			bW.newLine();
			bW.newLine();
			bW.close();
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	} //Cierre del metodo gastosLog
	
	/**
     * Metodo para que los componentes de la ventana realicen una accion concreta.
     *  
     */
	@SuppressWarnings("unused")
	public void actionPerformed(ActionEvent evento) {
		// Si ha seleccionado radio "Efectivo"
		if(evento.getSource()==rbEfectivo){
			// Se establece "Efectivo" el valor de la variable 
			formaPago=rbEfectivo.getText();
			rbAmaia.setEnabled(false);
			rbPeio.setEnabled(false);
			nombreTarjeta="-";
			
		}
		// Si ha seleccionado radio "Tarjeta"
		if(evento.getSource()==rbTarjeta){
			// Se establece "Tarjeta" el valor de la variable 
			formaPago=rbTarjeta.getText();
			rbAmaia.setEnabled(true);
			rbPeio.setEnabled(true);
			
		}
		// Si ha seleccionado radio "Amaia"
		if(evento.getSource()==rbAmaia){
			// Se establece "Amaia" el valor de la variable 
			nombreTarjeta=rbAmaia.getText();
			
		}
		// Si ha seleccionado radio "Peio"
		if(evento.getSource()==rbPeio){
			// Se establece "Peio" el valor de la variable 
			nombreTarjeta=rbPeio.getText();
			
		}
		// Si hace clic en Otro, se van a�adiendo registros al array hasta que da al Menu
		if(evento.getSource()==btnOtroMas){
			
			try {				
				// Sentencia SQL SELECT
				sql = "SELECT * FROM gastos_tienda;";
				// Comprueba si la tabla esta vacia
				tablaVacia=BD.tablaVacia(stmt,sql);
				// Se a�aden los datos de cada componente al arraylist
				if(tablaVacia==true){
					id++;
					// Obtener los datos de cada componente de la ventana
					fecha=txtFecha.getText().trim();
					tienda=cbTienda.getSelectedItem().toString().trim();
					coste=Double.parseDouble(txtCoste.getText());
					// Se guardan los datos en fichero log
					gastosLog("A�adir",id,fecha,tienda,coste);
					//Se crea un nuevo objeto de gastoTiendas
					gastoTiendas=new Gastos_T(id,fecha,tienda,coste,formaPago,nombreTarjeta);					
					// Se a�ade cada objeto al arraylist
					gestorGastosT.anadir(gastoTiendas);
					// Se limpian los componentes
					limpiar();
				}
				else{
					cont++;
					// Cuando cont sea igual a 1 se obtiene la ID de la BD
					if(cont==1){
						// Se obtiene el ultimo ID desde la BD
						id=gestorGastosT.ultimoID();
					}					
					id++;
					// Obtener los datos de cada componente de la ventana
					fecha=txtFecha.getText().trim();
					tienda=cbTienda.getSelectedItem().toString().trim();
					coste=Double.parseDouble(txtCoste.getText());
					// Se guardan los datos en fichero log
					gastosLog("A�adir",id,fecha,tienda,coste);
					//Se crea un nuevo objeto de gastoTiendas
					gastoTiendas=new Gastos_T(id,fecha,tienda,coste,formaPago,nombreTarjeta);					
					// Se a�ade cada objeto al arraylist
					gestorGastosT.anadir(gastoTiendas);
					// Se limpian los componentes
					limpiar();
					
				} // Cierre del else				
			}
			catch (SQLException e) {
				// Muestra un error
				JOptionPane.showMessageDialog(null, e.getMessage(),"Error ",JOptionPane.ERROR_MESSAGE);
			}
		}		
		// Si hace clic en Menu se guardan los datos y vuelve a la principal
		if(evento.getSource()==btnMenu){ 
			
			try {
				if(txtFecha.getText().equals("")){
					// Se instancia el controlador Principal
			  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
			  		// Desparece la ventana a�adir Gastos de Tienda
			  		setVisible(false);
				}
				else{
					cont++;
					// Cuando cont sea igual a 1 se obtiene la ID de la BD
					if(cont==1){
						// Se obtiene el ultimo ID desde la BD
						id=gestorGastosT.ultimoID();
					}
					id++;
					// Obtener los datos de cada componente de la ventana
					fecha=txtFecha.getText().trim();
					tienda=cbTienda.getSelectedItem().toString().trim();
					coste=Double.parseDouble(txtCoste.getText());
					if(formaPago.equals("")){
						JOptionPane.showMessageDialog(null, "Seleccione la forma de pago");
					}
					// Se guardan los datos en fichero log
					gastosLog("A�adir",id,fecha,tienda,coste);
					//Se crea un nuevo objeto de gastoTiendas
					gastoTiendas=new Gastos_T(id,fecha,tienda,coste,formaPago,nombreTarjeta);				
					// Se a�ade cada objeto al arraylist
					gestorGastosT.anadir(gastoTiendas);
					//JOptionPane.showMessageDialog(null,gestorGastosT.listar());
					// Inserta los datos del array en la BD
					gestorGastosT.insertarBD();
					// Muestra un aviso para el usuario
					JOptionPane.showMessageDialog(null, "Los datos han sido guardados correctamente.");
					// Se instancia el controlador Principal
			  		ControladorPrincipal controladorPrincipal=new ControladorPrincipal();
			  		// Desparece la ventana a�adir Gastos de Tienda
			  		setVisible(false);
				} // Cierre del else
			}
		  	catch (SQLException e) {
				// Muestra un error
				JOptionPane.showMessageDialog(null, e.getMessage(),"Error ",JOptionPane.ERROR_MESSAGE);
			}			
		}// Cierre evento btnMenu
		
	} // Cierre del metodo actionPerformed
} // Cierre de la clase